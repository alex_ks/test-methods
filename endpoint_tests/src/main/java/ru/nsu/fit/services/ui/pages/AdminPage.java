package ru.nsu.fit.services.ui.pages;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.services.browser.Browser;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by alexander on 12/25/16.
 */
public class AdminPage extends PageBase {
    private static final Logger logger = LoggerFactory.getLogger("AdminPage");

    public static final String CUSTOMERS_FRAME = "customers_frame_id";
    public static final String PLANS_FRAME = "plans_frame_id";
    public static final String ADD_CUSTOMER_BUTTON = "add_new_customer";
    public static final String ADD_PLAN_BUTTON = "add_new_plan";
    public static final String LOGOUT_BUTTON = "log_out_id";

    public AdminPage(Browser browser) {
        super(browser);
    }

    @Step("Clicking add customer button")
    public void clickAddCustomer() {
        logger.info("Switching to customers frame");
        browser.switchToDefaultContent();
        browser.waitForElement(By.id(CUSTOMERS_FRAME));
        browser.switchToFrame(CUSTOMERS_FRAME);

        logger.info("Clicking button");
        browser.click(By.id(ADD_CUSTOMER_BUTTON));
    }

    @Step("Clicking add plan button")
    public void clickAddPlan() {
        logger.info("Switching to plans frame");
        browser.switchToDefaultContent();
        browser.waitForElement(By.id(PLANS_FRAME));
        browser.switchToFrame(PLANS_FRAME);

        logger.info("Clicking button");
        browser.click(By.id(ADD_PLAN_BUTTON));
    }

    @Step("Logging out from admin page")
    public void clickLogout() {
        logger.info("Switching to default content");
        browser.switchToDefaultContent();
        browser.waitForElement(By.id(LOGOUT_BUTTON));
        logger.info("Clicking button");
        browser.click(By.id(LOGOUT_BUTTON));
    }
}
