package ru.nsu.fit.services.browser;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.ImageUtils;

import java.io.Closeable;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Please read: https://github.com/SeleniumHQ/selenium/wiki/Grid2
 *
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Browser implements Closeable {
    private static final Logger logger = LoggerFactory.getLogger("Browser");
    private WebDriver webDriver;

    public Browser(Platform platform, BrowserType type) {
        // create web driver
        try {
            if (platform == Platform.LINUX64) {
                System.setProperty("webdriver.chrome.driver", "./lib/chromedriver");
                System.setProperty("webdriver.gecko.driver", "./lib/geckodriver");
            } else {
                System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");
                System.setProperty("webdriver.gecko.driver", "./lib/geckodriver.exe");
            }

            if (type == BrowserType.CHROME) {
                webDriver = new ChromeDriver();
                logger.info("Launching Chrome");
            } else {
                webDriver = new FirefoxDriver();
                logger.info("Launching Firefox");
            }

            webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    public Browser openPage(String url) {
        makeScreenshot(before("Opening", url));
        logger.info("Opening page '%s'", url);
        webDriver.get(url);
        return this;
    }

    public Browser waitForElement(By element) {
        logger.info("Waiting for element %s", element.toString());
        return waitForElement(element, 10);
    }

    public Browser waitForElement(final By element, int timeoutSec) {
        logger.info("Waiting for element %s with timeout %d",
                    element.toString(),
                    timeoutSec);
        makeScreenshot(before("Waiting", element));
        WebDriverWait wait = new WebDriverWait(webDriver, timeoutSec);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        makeScreenshot(after("Waiting", element));
        return this;
    }

    public Browser click(By element) {
        logger.info("Click element %s", element.toString());
        makeScreenshot(before("Clicking", element));
        webDriver.findElement(element).click();
        makeScreenshot(after("Clicking", element));
        return this;
    }

    public Browser typeText(By element, String text) {
        logger.info("Type text '%s' to element %s", text, element.toString());
        makeScreenshot(before("Typing to", element));
        webDriver.findElement(element).sendKeys(text);
        makeScreenshot(after("Typing to", element));
        return this;
    }

    public Browser switchToFrame(String frameId) {
        logger.info("Switching to frame %s", frameId);
        makeScreenshot(before("Switching to", frameId));
        webDriver.switchTo().frame(frameId);
        makeScreenshot(after("Switching to", frameId));
        return this;
    }

    public Browser switchToDefaultContent() {
        logger.info("Switching to default content");
        makeScreenshot(before("Switching to", "default content"));
        webDriver.switchTo().defaultContent();
        makeScreenshot(after("Switching to", "default content"));
        return this;
    }

    public Alert getAlert() {
        logger.info("Trying to acquire alert");
        makeScreenshot(before("Switching to", "alert"));
        return webDriver.switchTo().alert();
    }

    public WebElement getElement(By element) {
        logger.info("Trying to find element %s", element.toString());
        makeScreenshot(before("Getting", element));
        return webDriver.findElement(element);
    }

    public String getValue(By element) {
        logger.info("Trying to get value of %s", element.toString());
        makeScreenshot(before("Getting value of", element));
        return getElement(element).getAttribute("value");
    }

    public List<WebElement> getElements(By element) {
        logger.info("Trying to find elements by %s", element);
        makeScreenshot(before("Getting all of", element));
        return webDriver.findElements(element);
    }

    public boolean isElementPresent(By element) {
        logger.info("Checking presence of element %s", element.toString());
        makeScreenshot(before("Checking presence of", element));
        return getElements(element).size() != 0;
    }

    public byte[] makeScreenshot(String name) {
        return AllureUtils
                .saveImageAttach(name + ".png",
                                 ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.BYTES));
    }

    private String before(String action, Object object) {
        return String.format("Before %s %s", action, object.toString());
    }

    private String after(String action, Object object) {
        return String.format("After %s %s", action, object.toString());
    }

    @Override
    public void close() {
        logger.info("Closing");
        webDriver.close();
    }
}
