package ru.nsu.fit.services.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by alexander on 12/18/16.
 */
public class PlanDto extends DtoBase {
    @JsonProperty("Id")
    private String id;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Details")
    private String details;

    @JsonProperty("MaxSeats")
    private int maxSeats;

    @JsonProperty("MinSeats")
    private int minSeats;

    @JsonProperty("FeePerUnit")
    private int feePerUnit;

    @JsonProperty("OneTimeCost")
    private int oneTimeCost;

    @JsonProperty("IsExternal")
    private boolean isExternal = false;

    public PlanDto() {}

    public PlanDto(String id,
                   String name,
                   String details,
                   int minSeats,
                   int maxSeats,
                   int feePerUnit,
                   int oneTimeCost,
                   boolean isExternal) {
        setId(id);
        setName(name);
        setDetails(details);
        setMinSeats(minSeats);
        setMaxSeats(maxSeats);
        setFeePerUnit(feePerUnit);
        setOneTimeCost(oneTimeCost);
        setExternal(isExternal);
    }

    @Override
    public String toString() {
        return String.format("%s (%s), [%d, %d], %d per unit, %d once",
                             name,
                             details,
                             minSeats,
                             maxSeats,
                             feePerUnit,
                             oneTimeCost);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }

    public int getMinSeats() {
        return minSeats;
    }

    public void setMinSeats(int minSeats) {
        this.minSeats = minSeats;
    }

    public int getFeePerUnit() {
        return feePerUnit;
    }

    public void setFeePerUnit(int feePerUnit) {
        this.feePerUnit = feePerUnit;
    }

    public int getOneTimeCost() {
        return oneTimeCost;
    }

    public void setOneTimeCost(int oneTimeCost) {
        this.oneTimeCost = oneTimeCost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isExternal() {
        return isExternal;
    }

    public void setExternal(boolean external) {
        isExternal = external;
    }
}
