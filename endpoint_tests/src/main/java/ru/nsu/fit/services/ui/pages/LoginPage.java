package ru.nsu.fit.services.ui.pages;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.services.browser.Browser;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by alexander on 12/25/16.
 */
public class LoginPage extends PageBase {
    private static final Logger logger = LoggerFactory.getLogger("LoginPage");

    public static final String LOGIN_PAGE_ADDRESS = "http://localhost:8080/endpoint";
    public static final String USERNAME_INPUT = "username";
    public static final String PASSWORD_INPUT = "password";
    public static final String LOGIN_BUTTON = "login";

    public LoginPage(Browser browser) {
        super(browser);
    }

    @Step("Opening page {0}")
    public static LoginPage openPage(String url, Browser browser) {
        browser.openPage(url);
        browser.waitForElement(By.id(USERNAME_INPUT));
        return new LoginPage(browser);
    }

    @Step("Filling username with {0}")
    public void fillUsername(String login) {
        browser.typeText(By.id(USERNAME_INPUT), login);
    }

    @Step("Filling password with {0}")
    public void fillPassword(String password) {
        browser.typeText(By.id(PASSWORD_INPUT), password);
    }

    @Step("Submitting form")
    public void submitForm() {
        browser.click(By.id(LOGIN_BUTTON));
    }
}
