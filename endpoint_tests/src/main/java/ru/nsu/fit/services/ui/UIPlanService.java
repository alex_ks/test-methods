package ru.nsu.fit.services.ui;

import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.rest.CustomerService;
import ru.nsu.fit.services.rest.dto.PlanDto;
import ru.nsu.fit.services.ui.pages.AdminPage;
import ru.nsu.fit.services.ui.pages.LoginPage;
import ru.nsu.fit.services.ui.pages.PlanCreationPage;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by alexander on 12/25/16.
 */
public class UIPlanService {
    @Step("Forwarding to plan creation page")
    public static PlanCreationPage goToPlanCreation(AdminPage page) {
        page.clickAddPlan();
        return new PlanCreationPage(page.getBrowser());
    }

    @Step("Creating plan {1}")
    public static void createPlan(PlanCreationPage page, PlanDto plan) {
        page.fillName(plan.getName());
        page.fillDetails(plan.getDetails());
        page.fillMinSeats(plan.getMinSeats());
        page.fillMaxSeats(plan.getMaxSeats());
        page.fillFeePerUnit(plan.getFeePerUnit());
        page.fillOneTimeCost(plan.getOneTimeCost());
        page.submitForm();
    }
}
