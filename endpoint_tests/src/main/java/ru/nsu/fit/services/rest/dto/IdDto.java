package ru.nsu.fit.services.rest.dto;

/**
 * @author Alina Mozhina (mohinalina@gmail.com).
 */
public class IdDto {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
