package ru.nsu.fit.services.ui.pages;

import ru.nsu.fit.services.browser.Browser;

/**
 * Created by alexander on 12/25/16.
 */
public class PageBase {
    protected Browser browser;

    public PageBase(Browser browser) {
        this.browser = browser;
    }

    public Browser getBrowser() {
        return browser;
    }
}
