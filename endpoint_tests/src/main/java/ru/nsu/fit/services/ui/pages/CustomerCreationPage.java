package ru.nsu.fit.services.ui.pages;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.services.browser.Browser;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by alexander on 12/25/16.
 */
public class CustomerCreationPage extends PageBase {
    private static final Logger logger = LoggerFactory.getLogger("CustomerCreationPage");

    public static final String FIRST_NAME_INPUT = "first_name_id";
    public static final String LAST_NAME_INPUT = "last_name_id";
    public static final String EMAIL_INPUT = "email_id";
    public static final String PASSWORD_INPUT = "password_id";
    public static final String CREATE_CUSTOMER_BUTTON = "create_customer_id";

    public CustomerCreationPage(Browser browser) {
        super(browser);
        browser.waitForElement(By.id(FIRST_NAME_INPUT));
    }

    @Step("Filling first name with {0}")
    public void fillFirstName(String name) {
        browser.typeText(By.id(FIRST_NAME_INPUT), name);
    }

    @Step("Filling last name with {0}")
    public void fillLastName(String lastName) {
        browser.typeText(By.id(LAST_NAME_INPUT), lastName);
    }

    @Step("Filling email with {0}")
    public void fillEmail(String email) {
        browser.typeText(By.id(EMAIL_INPUT), email);
    }

    @Step("Filling password with {0}")
    public void fillPassword(String password) {
        browser.typeText(By.id(PASSWORD_INPUT), password);
    }

    @Step("Submitting customer creation form")
    public void submitForm() {
        browser.click(By.id(CREATE_CUSTOMER_BUTTON));
    }
}
