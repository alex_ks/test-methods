package ru.nsu.fit.services.rest;

import org.apache.http.HttpStatus;
import ru.nsu.fit.services.rest.dto.PlanDto;
import ru.nsu.fit.services.utils.WebClientService;
import ru.nsu.fit.shared.JsonMapper;
import ru.yandex.qatools.allure.annotations.Step;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import java.util.List;

import static ru.nsu.fit.services.rest.CustomerService.ADMIN_LOGIN;
import static ru.nsu.fit.services.rest.CustomerService.ADMIN_PASSWORD;
import static ru.nsu.fit.services.rest.RestResources.CUSTOMER_ROOT;
import static ru.nsu.fit.services.rest.RestResources.PLAN_DELETE;
import static ru.nsu.fit.services.rest.RestResources.PLAN_ROOT;

/**
 * Created by alexander on 12/18/16.
 */
public class PlanService {
    public static Response createPlanHttp(PlanDto plan) {
        WebTarget webTarget = WebClientService.getWebTarget(ADMIN_LOGIN, ADMIN_PASSWORD, PLAN_ROOT);
        return WebClientService.postDto(webTarget, plan);
    }

    @Step("Creating plan {0}")
    public static void createPlan(PlanDto plan) {
        Response response = createPlanHttp(plan);
        if (response.getStatus() != HttpStatus.SC_OK)
            throw new RuntimeException(response.readEntity(String.class));
    }

    public static Response deletePlanHttp(String name) {
        WebTarget webTarget = WebClientService.getWebTarget(ADMIN_LOGIN,
                                                            ADMIN_PASSWORD,
                                                            String.format(PLAN_DELETE, name));

        return WebClientService.delete(webTarget);
    }

    @Step("Deleting plan {0}")
    public static void deletePlan(String name) {
        Response response = deletePlanHttp(name);
        if (response.getStatus() != HttpStatus.SC_OK)
            throw new RuntimeException(response.readEntity(String.class));
    }

    public static Response getAllPlansHttp() {
        WebTarget webTarget = WebClientService.getWebTarget(ADMIN_LOGIN,
                                                            ADMIN_PASSWORD,
                                                            PLAN_ROOT);

        return WebClientService.get(webTarget);
    }

    @Step("Getting all existing plans")
    public static PlanDto[] getAllPlans() {
        Response response = getAllPlansHttp();
        return JsonMapper.fromJson(response.readEntity(String.class), PlanDto[].class);
    }
}
