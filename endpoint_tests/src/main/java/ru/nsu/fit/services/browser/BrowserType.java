package ru.nsu.fit.services.browser;

/**
 * Created by alexander on 12/18/16.
 */
public enum BrowserType {
    CHROME,
    FIREFOX
}
