package ru.nsu.fit.services.ui;

import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.rest.CustomerService;
import ru.nsu.fit.services.rest.dto.CustomerDto;
import ru.nsu.fit.services.ui.pages.AdminPage;
import ru.nsu.fit.services.ui.pages.CustomerCreationPage;
import ru.nsu.fit.services.ui.pages.LoginPage;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by alexander on 12/25/16.
 */
public class UICustomerService {
    @Step("Opening site to test")
    public static LoginPage openSite(Browser browser) {
        return LoginPage.openPage(LoginPage.LOGIN_PAGE_ADDRESS, browser);
    }

    @Step("Logging in as admin")
    public static AdminPage loginAsAdmin(LoginPage page) {
        page.fillUsername(CustomerService.ADMIN_LOGIN);
        page.fillPassword(CustomerService.ADMIN_PASSWORD);
        page.submitForm();
        return new AdminPage(page.getBrowser());
    }

    @Step("Forwarding to customer creation page")
    public static CustomerCreationPage goToCustomerCreation(AdminPage page) {
        page.clickAddCustomer();
        return new CustomerCreationPage(page.getBrowser());
    }

    @Step("Creating customer {1}")
    public static void createCustomer(CustomerCreationPage page, CustomerDto customer) {
        page.fillFirstName(customer.getFirstName());
        page.fillLastName(customer.getLastName());
        page.fillEmail(customer.getLogin());
        page.fillPassword(customer.getPass());
        page.submitForm();
    }
}
