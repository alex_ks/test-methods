package ru.nsu.fit.services.ui.pages;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by alexander on 12/25/16.
 */
public class PlanCreationPage extends PageBase {
    public static final String NAME_INPUT = "name_id";
    public static final String DETAILS_INPUT = "details_id";
    public static final String MIN_SEATS_INPUT = "min_seats_id";
    public static final String MAX_SEATS_INPUT = "max_seats_id";
    public static final String FEE_PER_UNIT_INPUT = "fee_per_unit_id";
    public static final String ONE_TIME_COST_INPUT = "one_time_cost_id";
    public static final String CREATE_PLAN_BUTTON = "create_plan_id";

    public PlanCreationPage(Browser browser) {
        super(browser);
    }

    @Step("Filling name with {0}")
    public void fillName(String name) {
        browser.typeText(By.id(NAME_INPUT), name);
    }

    @Step("Filling details with {0}")
    public void fillDetails(String details) {
        browser.typeText(By.id(DETAILS_INPUT), details);
    }

    @Step("Filling min seats with {0}")
    public void fillMinSeats(int minSeats) {
        browser.typeText(By.id(MIN_SEATS_INPUT), String.valueOf(minSeats));
    }

    @Step("Filling max seats with {0}")
    public void fillMaxSeats(int maxSeats) {
        browser.typeText(By.id(MAX_SEATS_INPUT), String.valueOf(maxSeats));
    }

    @Step("Filling fee per unit with {0}")
    public void fillFeePerUnit(int feePerUnit) {
        browser.typeText(By.id(FEE_PER_UNIT_INPUT), String.valueOf(feePerUnit));
    }

    @Step("Filling one time cost with {0}")
    public void fillOneTimeCost(int oneTimeCost) {
        browser.typeText(By.id(ONE_TIME_COST_INPUT), String.valueOf(oneTimeCost));
    }

    @Step("Submitting plan creation form")
    public void submitForm() {
        browser.click(By.id(CREATE_PLAN_BUTTON));
    }
}
