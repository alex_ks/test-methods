package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.rest.UserService;
import ru.nsu.fit.services.rest.dto.IdDto;
import ru.nsu.fit.services.rest.dto.UserDto;
import ru.nsu.fit.services.utils.WebClientService;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.DataGenerator;
import ru.nsu.fit.shared.JsonMapper;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.core.Response;

/**
 * @author Alina Mozhina (mohinalina@gmail.com).
 */
public class CreateUserTest extends CreateCustomerTest {
    protected String userId;
    protected UserDto user;

    @BeforeClass
    public void initUserData() {
        user = DataGenerator.getUserDto();
    }

    @Test(dependsOnMethods = { "login" })
    @Step("Create user")
    @Description("Create user via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("User creation feature")
    public void createUser() {
        Response response = UserService.createUser(customer.getLogin(), customer.getPass(), customerId, user);

        Assert.assertEquals(response.getStatus(), 200);
        AllureUtils.saveTextLog(String.format(TestResources.USER_CREATED, customer.getLogin(), user.getLogin()));
    }

    @Test(dependsOnMethods = { "createUser" })
    @Step("Check login")
    @Description("Get userId by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("User authentication feature")
    public void loginUser() {
        Response response = UserService.login(user.getLogin(), user.getPassword());

        Assert.assertEquals(response.getStatus(), 200);

        String idJson = response.readEntity(String.class);
        IdDto idDto = JsonMapper.fromJson(idJson, IdDto.class);
        userId = idDto.getId();

        AllureUtils.saveTextLog(String.format(TestResources.USER_LOGGED_IN, user.getLogin(), userId));
    }

    @AfterClass
    @Step("Delete user")
    public void deleteUser() {
        Response response = UserService.deleteUser(customer.getLogin(), customer.getPass(), user.getLogin());

        Assert.assertEquals(response.getStatus(), 200);
        AllureUtils.saveTextLog(String.format(TestResources.USER_DELETED, user.getLogin(), customer.getLogin()));
    }
}
