package ru.nsu.fit.tests.UI;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.rest.CustomerService;
import ru.nsu.fit.services.rest.dto.CustomerDto;
import ru.nsu.fit.services.rest.dto.IdDto;
import ru.nsu.fit.services.ui.UICustomerService;
import ru.nsu.fit.services.ui.pages.AdminPage;
import ru.nsu.fit.services.ui.pages.CustomerCreationPage;
import ru.nsu.fit.services.ui.pages.LoginPage;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.DataGenerator;
import ru.nsu.fit.shared.JsonMapper;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Title("Create customers via UI")
public class UICreateCustomerTest {
    private static final Logger logger = LoggerFactory.getLogger("UICreateCustomerTest");
    private Browser browser = null;
    private CustomerDto customerDavid = null;
    private CustomerDto customerKonrad = null;

    @BeforeClass
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();
        customerDavid = DataGenerator.getCustomerDto();
        customerDavid.setFirstName("David");
        customerKonrad = DataGenerator.getCustomerDto();
        customerKonrad.setFirstName("Konrad");
    }

    @Test
    @Title("Create customers")
    @Description("Create customers via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer feature")
    public void createCustomers() {
        // login to admin cp
        logger.info("Loading login form");
        LoginPage loginPage = UICustomerService.openSite(browser);

        logger.info("Trying to log as admin");
        AdminPage adminPage = UICustomerService.loginAsAdmin(loginPage);

        // create customer David
        logger.info("Creating customer" + customerDavid.getFirstName());
        CustomerCreationPage creationPage = UICustomerService.goToCustomerCreation(adminPage);
        UICustomerService.createCustomer(creationPage, customerDavid);

        // create customer Konrad
        logger.info("Creating customer" + customerKonrad.getFirstName());
        creationPage = UICustomerService.goToCustomerCreation(adminPage);
        UICustomerService.createCustomer(creationPage, customerKonrad);
    }

    @Test(dependsOnMethods = { "createCustomers" })
    @Title("Check login")
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer feature")
    public void checkCustomers() {
        Assert.assertTrue(CustomerService.checkCustomerExists(customerDavid.getLogin(),
                                                              customerDavid.getPass()));
        Assert.assertTrue(CustomerService.checkCustomerExists(customerKonrad.getLogin(),
                                                              customerKonrad.getPass()));
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (browser != null) {
            browser.close();
        }
        deleteCustomer(customerDavid.getFirstName(), customerDavid);
        deleteCustomer(customerKonrad.getFirstName(), customerKonrad);
    }

    @Step("Delete customer {0}")
    public void deleteCustomer(String name, CustomerDto customer) {
        String idJson = CustomerService.loginHttp(customer.getLogin(), customer.getPass())
                .readEntity(String.class);
        IdDto idDto = JsonMapper.fromJson(idJson, IdDto.class);
        CustomerService.deleteCustomerHttp(idDto.getId());
    }
}
