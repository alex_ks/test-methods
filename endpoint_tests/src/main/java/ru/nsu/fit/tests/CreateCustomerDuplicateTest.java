package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.rest.CustomerService;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.core.Response;

import static ru.nsu.fit.tests.TestResources.CUSTOMER_ALREADY_EXISTS;

/**
 * @author Alina Mozhina (mohinalina@gmail.com).
 */
public class CreateCustomerDuplicateTest extends CreateCustomerTest {
    @Test(dependsOnMethods = { "login" })
    @Severity(SeverityLevel.NORMAL)
    @Step("Create duplicate customer")
    @Description("Check whether not unique login is forbidden")
    public void createDuplicateCustomer() {
        Response response = CustomerService.createCustomerHttp(customer);

        Assert.assertEquals(response.getStatus(), 400);
        AllureUtils.saveTextLog(CUSTOMER_ALREADY_EXISTS, customer.getLogin());
    }
}
