package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.rest.CustomerService;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.core.Response;

/**
 * @author Alina Mozhina (mohinalina@gmail.com).
 */
public class TopUpCustomerBalanceTest extends CreateCustomerTest {
    @Test(dependsOnMethods = { "login" })
    @Step("Top up customer balance")
    @Description("Top up customer balance by positive value")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer balance topping up")
    public void topUpCustomerBalance() {
        int topUpValue = 500;
        Response response = CustomerService.topUpBalanceHttp(customer.getLogin(), customer.getPass(),
                                                         customerId, topUpValue);

        Assert.assertEquals(response.getStatus(), 200);
        AllureUtils.saveTextLog(String.format(TestResources.CUSTOMER_BALANCE_TOPPED_UP,
                                              customer.getLogin(), topUpValue));
    }
}
