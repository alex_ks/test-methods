package ru.nsu.fit.tests;

/**
 * @author Alina Mozhina (mohinalina@gmail.com).
 */
class TestResources {
    static final String CUSTOMER_CREATED = "Created customer: ";
    static final String CUSTOMER_LOGGED_IN = "Customer %s has logged in successfully: customerId = %s";
    static final String CUSTOMER_DELETED = "Deleted customer: customerId = ";
    static final String CUSTOMER_ALREADY_EXISTS = "Customer with login %s already exists";
    static final String CUSTOMER_BALANCE_TOPPED_UP = "Customer %s topped up balance by %d";

    static final String USER_CREATED = "Created user %s by %s";
    static final String USER_LOGGED_IN = "Customer %s has logged in successfully: userId = %s";
    static final String USER_DELETED = "Deleted user %s of %s";
}
