package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.rest.CustomerService;
import ru.nsu.fit.services.rest.dto.CustomerDto;
import ru.nsu.fit.services.rest.dto.IdDto;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.DataGenerator;
import ru.nsu.fit.shared.JsonMapper;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.core.Response;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Title("Create customer test")
public class CreateCustomerTest {
    protected String customerId;
    protected CustomerDto customer;

    @BeforeClass
    public void initCustomerData() {
        customer = DataGenerator.getCustomerDto();
    }

    @Test
    @Step("Create customer")
    @Description("Create customer via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer creation feature")
    public void createCustomer() {
        Response response = CustomerService.createCustomerHttp(customer);
        Assert.assertEquals(response.getStatus(), 200);
        AllureUtils.saveTextLog(TestResources.CUSTOMER_CREATED + response.readEntity(String.class));
    }

    @Test(dependsOnMethods = { "createCustomer" })
    @Step("Check login")
    @Description("Get customer customerId by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer authentication feature")
    public void login() {
        Response response = CustomerService.loginHttp(customer.getLogin(), customer.getPass());

        String idJson = response.readEntity(String.class);
        System.err.println(idJson);
        Assert.assertEquals(response.getStatus(), 200);
        IdDto idDto = JsonMapper.fromJson(idJson, IdDto.class);
        customerId = idDto.getId();

        AllureUtils.saveTextLog(String.format(TestResources.CUSTOMER_LOGGED_IN, customer.getLogin(), customerId));
    }

    @AfterClass
    public void deleteCustomer() {
        Response response = CustomerService.deleteCustomerHttp(customerId);

        Assert.assertEquals(response.getStatus(), 200);
        AllureUtils.saveTextLog(TestResources.CUSTOMER_DELETED + customerId);
    }
}
