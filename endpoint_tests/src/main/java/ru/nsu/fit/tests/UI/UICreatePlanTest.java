package ru.nsu.fit.tests.UI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.rest.PlanService;
import ru.nsu.fit.services.rest.dto.PlanDto;
import ru.nsu.fit.services.ui.UICustomerService;
import ru.nsu.fit.services.ui.UIPlanService;
import ru.nsu.fit.services.ui.pages.AdminPage;
import ru.nsu.fit.services.ui.pages.LoginPage;
import ru.nsu.fit.services.ui.pages.PlanCreationPage;
import ru.nsu.fit.shared.DataGenerator;
import ru.nsu.fit.shared.JsonMapper;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.Arrays;

/**
 * Created by alexander on 12/18/16.
 */

@Title("Create plans via UI")
public class UICreatePlanTest {
    private static final Logger logger = LoggerFactory.getLogger("UICreatePlanTest");

    protected Browser browser = null;
    protected PlanDto planSpotify = null;
    protected PlanDto planReSharper = null;


    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        browser = BrowserService.openNewBrowser();

        planSpotify = new PlanDto(null, DataGenerator.getCompanyName(), "Listen music everywhere", 1, 5, 5, 10, false);
        planReSharper = new PlanDto(null, DataGenerator.getCompanyName(), "C# Unlimited", 1, 5, 100, 150, false);
    }

    @Title("Create plans")
    @Description("Create plans via UI API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Plan feature")
    @Test
    public void createPlans() {
        // login to admin cp
        logger.info("Loading login form");
        LoginPage loginPage = UICustomerService.openSite(browser);

        logger.info("Trying to log as admin");
        AdminPage adminPage = UICustomerService.loginAsAdmin(loginPage);

        logger.info("Creating plan" + planSpotify.getName());
        PlanCreationPage creationPage = UIPlanService.goToPlanCreation(adminPage);
        UIPlanService.createPlan(creationPage, planSpotify);

        logger.info("Creating plan" + planReSharper.getName());
        creationPage = UIPlanService.goToPlanCreation(adminPage);
        UIPlanService.createPlan(creationPage, planReSharper);
    }

    @Test(dependsOnMethods = { "createPlans" })
    @Title("Check plans")
    @Description("Find plans in table")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Plan feature")
    public void checkPlans() {
        PlanDto[] allPlans = PlanService.getAllPlans();
        checkPlanByName(planSpotify.getName(), allPlans);
        checkPlanByName(planReSharper.getName(), allPlans);
    }

    @Step("Searching for plan {0}")
    public void checkPlanByName(String name, PlanDto[] allPlans) {
        Assert.assertTrue(Arrays.stream(allPlans).anyMatch((x) -> x.getName().equals(name)));
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (browser != null)
            browser.close();
        PlanService.deletePlan(planSpotify.getName());
        PlanService.deletePlan(planReSharper.getName());
    }
}
