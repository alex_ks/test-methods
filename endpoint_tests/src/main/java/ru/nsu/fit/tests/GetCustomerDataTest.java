package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.rest.CustomerService;
import ru.nsu.fit.services.rest.dto.CustomerDto;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.core.Response;

/**
 * @author Alina Mozhina (mohinalina@gmail.com).
 */
@Title("Get customer data test")
public class GetCustomerDataTest extends CreateCustomerTest {

    @Test(dependsOnMethods = { "login" })
    @Severity(SeverityLevel.NORMAL)
    @Step("Check customer data")
    @Description("Get customer data and check whether it is valid")
    public void getCustomerData() {
        Response response = CustomerService.getCustomerDataHttp(customer.getLogin(), customer.getPass(), customerId);

        Assert.assertEquals(response.getStatus(), 200);

        CustomerDto customerDto = response.readEntity(CustomerDto.class);
        Assert.assertEquals(customerDto, customer);
    }
}
