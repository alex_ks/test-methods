package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.UUID;

/**
 * Created by alexander on 09.10.16.
 */
public class SubscriptionTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCorrectSubscription() {
        expectedEx = ExpectedException.none();
        new Subscription(UUID.randomUUID(),
                        UUID.randomUUID(),
                        UUID.randomUUID(),
                        10);
    }

    @Test
    public void testMaxSeatsMaximal() {
        expectedEx = ExpectedException.none();
        new Subscription(UUID.randomUUID(),
                        UUID.randomUUID(),
                        UUID.randomUUID(),
                        10);
    }

    @Test
    public void testMinSeatsMinimum() {
        expectedEx = ExpectedException.none();
        new Subscription(UUID.randomUUID(),
                        UUID.randomUUID(),
                        UUID.randomUUID(),
                        10);
    }

    @Test
    public void testMinSeatsMoreThanAllowed() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.MIN_SEATS_VALUE_OUT_OF_RANGE);
        new Subscription(UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                10);
    }

    @Test
    public void testMaxSeatsLessThanMinSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.MAX_SEATS_VALUE_OUT_OF_RANGE);
        new Subscription(UUID.randomUUID(),
                        UUID.randomUUID(),
                        UUID.randomUUID(),
                        10);
    }

    @Test
    public void testNegativeUsedSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.USED_SEATS_VALUE_OUT_OF_RANGE);
        new Subscription(UUID.randomUUID(),
                        UUID.randomUUID(),
                        UUID.randomUUID(),
                        -7);
    }
}
