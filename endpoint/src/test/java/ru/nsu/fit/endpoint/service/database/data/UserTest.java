package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.UUID;

/**
 * Created by alexander on 09.10.16.
 */
public class UserTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testUserNameContainsSpaces() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_FIRST_NAME);
        new User(UUID.randomUUID(),
                 "Erich Maria",
                 "Remarque",
                 "remarque98@gmail.com",
                 "ImWesten@929",
                 User.UserRole.USER);
    }

    @Test
    public void testUserNameNotCapital() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_FIRST_NAME);
        new User(UUID.randomUUID(),
                 "pasha",
                 "Lupkin",
                 "pashasoft@yandex.ru",
                 "Pablo#92",
                 User.UserRole.USER);
    }

    @Test
    public void testUserLastNameSeveralCapitals() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_LAST_NAME);
        new User(UUID.randomUUID(),
                 "John",
                 "VonNeumann",
                 "neumann@priston.ru",
                 "Quanten.Me4",
                 User.UserRole.USER);
    }

    @Test
    public void testUserLastNameInvalidSymbols() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_LAST_NAME);
        new User(UUID.randomUUID(),
                 "Nikolay",
                 "Karpov!",
                 "karpov@mail.ru",
                 "Shoygu$1l07",
                 User.UserRole.COMPANY_ADMINISTRATOR);
    }

    @Test
    public void testUserLoginNoDomain() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_EMAIL);
        new User(UUID.randomUUID(),
                 "User",
                 "Unknown",
                 "user@yandex",
                 "No0neKnow$",
                 User.UserRole.USER);
    }

    @Test
    public void testUserPasswordContainsName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.NAME_DEPENDENT_PASSWORD);
        new User(UUID.randomUUID(),
                 "Pavel",
                 "Lupkin",
                 "pashasoft@yandex.ru",
                 "Pavel0208",
                 User.UserRole.USER);
    }
}
