package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerTest {
    @Rule
        public ExpectedException expectedEx = ExpectedException.none();

        @Test
        public void testCustomerNameContainsSpaces() {
            expectedEx.expect(IllegalArgumentException.class);
            expectedEx.expectMessage(DataResources.INVALID_FIRST_NAME);
            new Customer.CustomerData("Erich Maria",
                                      "Remarque",
                                      "remarque98@gmail.com",
                                      "ImWesten@929",
                                      500);
    }

    @Test
    public void testCustomerNameNotCapital() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_FIRST_NAME);
        new Customer.CustomerData("pasha",
                                  "Lupkin",
                                  "pashasoft@yandex.ru",
                                  "Pablo#92",
                                  125);
    }

    @Test
    public void testCustomerLastNameSeveralCapitals() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_LAST_NAME);
        new Customer.CustomerData("John",
                                  "VonNeumann",
                                  "neumann@priston.ru",
                                  "Quanten.Me4",
                                  1000);
    }

    @Test
    public void testCustomerLastNameInvalidSymbols() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_LAST_NAME);
        new Customer.CustomerData("Nikolay",
                                  "Karpov!",
                                  "karpov@mail.ru",
                                  "Shoygu$1l07",
                                  250);
    }

    @Test
    public void testCustomerPasswordContainsName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.NAME_DEPENDENT_PASSWORD);
        new Customer.CustomerData("Pavel",
                                  "Lupkin",
                                  "pashasoft@yandex.ru",
                                  "Pavel0208",
                                  125);
    }

    @Test
    public void testCustomerNegativeBalance() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.NEGATIVE_MONEY);
        new Customer.CustomerData("Pavel",
                                  "Lupkin",
                                  "pashasoft@yandex.ru",
                                  "D@mnCredit0r",
                                  -100);
    }
}
