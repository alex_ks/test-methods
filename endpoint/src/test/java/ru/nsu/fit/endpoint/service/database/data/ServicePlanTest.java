package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.DataResources;
import ru.nsu.fit.endpoint.service.database.data.Plan;

/**
 * Created by alexander on 03.10.16.
 */
public class ServicePlanTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testTooShortPlanName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_SERVICE_PLAN_NAME_LENGTH);
        new Plan("A", "No details", 10, 20, 10);
    }

    @Test
    public void testPlanNameContainsSharp() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_SERVICE_PLAN_NAME_SYMBOLS);
        new Plan("Plan #", "No details", 10, 20, 10);
    }

    @Test
    public void testPlanNameContainsPercent() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_SERVICE_PLAN_NAME_SYMBOLS);
        new Plan("Plan %", "No details", 10, 20, 10);
    }

    @Test
    public void testPlanNameContainsExclamations() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_SERVICE_PLAN_NAME_SYMBOLS);
        new Plan("Plan !", "No details", 10, 20, 10);
    }

    @Test
    public void testTooShortPlanDetails() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.INVALID_SERVICE_PLAN_DETAILS_LENGTH);
        new Plan("Plan", "", 10, 20, 10);
    }

    @Test
    public void testMaxSeatsMaximal() {
        expectedEx = ExpectedException.none();
        new Plan("Plan", "No details", 10, 999999, 10);
    }

    @Test
    public void testMaxSeatsLessThanMinSeats() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.MAX_SEATS_VALUE_OUT_OF_RANGE);
        new Plan("Plan", "No details", 999999, 1, 10);
    }

    @Test
    public void testFeePerUnitLessThanMinimal() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage(DataResources.FEE_PER_UNIT_OUT_OF_RANGE);
        new Plan("Plan", "No details", 10, 20, -1);
    }
}
