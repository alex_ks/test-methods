/**
    * @author Alina Mozhina (mohinalina@gmail.com).
    */
$(document).ready(function(){
    $("#add_new_user").click(function() {
        $.redirect('add_user.html', null, 'GET', '_top');
    });

    $.get({
        url: api.userList(Cookies.get('id')),
        headers: {
            'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass'))
        }
    }).done(function(data) {
        var dataSet = [];
        for(var i = 0; i < data.length; i++) {
            var obj = data[i];
            dataSet.push([obj.firstName, obj.lastName, obj.login, obj.userRole])
        }

        $('#user_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { title: "First Name" },
                    { title: "Last Name" },
                    { title: "Email" },
                    { title: "Role" }
                ]
            });
    });
});