$(document).ready(function(){
    $("#add_new_customer").click(function() {
        $.redirect('/endpoint/add_customer.html', null, 'GET', '_top');
    });

    $.get({
        url: api.customerList,
        headers: {
            'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass'))
        }
    }).done(function(data) {
        var json = $.parseJSON(data);

        var dataSet = [];
        for(var i = 0; i < json.length; i++) {
            var obj = json[i];
            dataSet.push([obj.firstName, obj.lastName, obj.login, obj.pass, obj.money])
        }

        //$("#customer_list_id").html(data);
        $('#customer_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { title: "First Name" },
                    { title: "Last Name" },
                    { title: "Email" },
                    { title: "Password" },
                    { title: "Money" }
                ]
            });
    });
});