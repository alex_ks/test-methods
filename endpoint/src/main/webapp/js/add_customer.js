$(document).ready(function(){
    $("#back_id").click(
        function () {
            $.redirect('admin.html', {}, 'GET');
        });
    $("#create_customer_id").click(
        function() {
            var fName = $("#first_name_id").val();
            var lName = $("#last_name_id").val();
            var email = $("#email_id").val();
            var password = $("#password_id").val();

            // check fields
            if(email =='' || password =='') {
                $('.field').addClass('broken-field');
                alert(res.emptyLoginData);
            } else {
                $.post({
                    url: api.customerRoot,
                    headers: {
                        'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass')),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        "firstName":fName,
                        "lastName":lName,
                        "login":email,
                        "pass":password,
                        "money":"0"
                    })
                }).done(function(data) {
                    $.redirect('admin.html', {}, 'GET');
                }).fail(function(jqXHR) {
                    if (jqXHR.status == 400) {
                        $('.field').addClass('broken-field');
                        alert((jqXHR.responseText.split('\n') || [])[0]);
                    }
                });
            }
        }
    );
});