$(document).ready(function(){
    $("#login").click(function(){
        var username = $("#username").val();
        var password = $("#password").val();
        // Checking for blank fields.
        if(username =='' || password =='') {
            $('.field').addClass('broken-field');
            alert(res.emptyLoginData);
        } else {
            $.get({
                url: 'rest/get_role',
                headers: {
                    'Authorization': 'Basic ' + btoa(username + ':' + password)
                }
            })
                .done(function(data) {
                    var dataObj = $.parseJSON(data);
                    Cookies.set('username', username);
                    Cookies.set('pass', password);
                    Cookies.set('role', dataObj['role']);
                    if(dataObj['role']=='ADMIN') {
                        $.redirect('/endpoint/admin.html', null, 'GET');
                    } else if (dataObj['role']=='CUSTOMER') {
                        $.get({
                            url: api.customerRoot + "/" + username,
                            headers: {
                                'Authorization': 'Basic ' + btoa(username + ':' + password)
                            }
                        }).done(function (data) {
                            Cookies.set('id', data.id);
                            $.redirect('/endpoint/customer.html', null, 'GET');
                        }).fail(function(jqXHR) {
                            alert((jqXHR.responseText.split('\n') || [])[0]);
                        });
                    }
                })
                .fail(function(jqXHR) {
                    $('.field').addClass('broken-field');
                    if (jqXHR.status == 401) {
                        alert(res.incorrectLoginData);
                        return;
                    }
                    alert((jqXHR.responseText.split('\n') || [])[0]);
                });
        }
    });
});