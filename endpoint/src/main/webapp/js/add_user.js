/**
    * @author Alina Mozhina (mohinalina@gmail.com).
    */
$(document).ready(function(){
    $("#create_user_id").click(
        function() {
            var fName = $("#first_name_id").val();
            var lName = $("#last_name_id").val();
            var email = $("#email_id").val();
            var password = $("#password_id").val();
            var role = $("#role_id").val();

            // check fields
            if(email =='' || password =='') {
                $('.field').addClass('broken-field');
                alert(res.emptyLoginData);
            } else {
                $.post({
                    url: api.userCreate(Cookies.get('id')),
                    headers: {
                        'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass')),
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        "firstName":fName,
                        "lastName":lName,
                        "login":email,
                        "password":password,
                        "userRole":role
                    })
                }).done(function(data) {
                    $.redirect('/endpoint/customer.html', null, 'GET');
                }).fail(function(jqXHR) {
                    if (jqXHR.status == 400) {
                        $('.field').addClass('broken-field');
                        alert((jqXHR.responseText.split('\n') || [])[0]);
                    }
                });
            }
        }
    );
});