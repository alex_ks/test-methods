/**
    * @author Alina Mozhina (mohinalina@gmail.com).
    */
$(document).ready(function(){
    $("#add_new_plan").click(function() {
        $.redirect('/endpoint/add_plan.html', {}, 'GET', '_top');
    });

    $.get({
        url: api.planList,
        headers: {
            'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass'))
        }
    }).done(function(plans) {
        var dataSet = [];
        for(var i = 0; i < plans.length; i++) {
            var plan = plans[i];
            dataSet.push([plan.Name, plan.Details, plan.MinSeats, plan.MaxSeats, plan.FeePerUnit, plan.OneTimeCost])
        }

        $('#plan_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { title: "Name" },
                    { title: "Details" },
                    { title: "Min Seats" },
                    { title: "Max Seats" },
                    { title: "Fee per unit" },
                    { title: "One time cost" }
                ]
            });
    });
});