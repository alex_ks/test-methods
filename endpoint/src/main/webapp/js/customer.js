/**
    * @author Alina Mozhina (mohinalina@gmail.com).
    */
$(document).ready(function(){
    function getCustomerData() {
        $.get({
            url: api.customerData(Cookies.get('id')),
            headers: {
                'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass'))
            }
        }).done(function(customerData) {
            var dataSet = [];
            dataSet.push([customerData.firstName, customerData.lastName, customerData.login, customerData.money]);

            $('#customer_info_id')
                .DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    data: dataSet,
                    columns: [
                        { title: "First Name" },
                        { title: "Last Name" },
                        { title: "Email" },
                        { title: "Money" }
                    ]
                });
        });
    }

    getCustomerData();

    $("#log_out_id").click(function () {
        Cookies.remove('username');
        Cookies.remove('password');
        $.redirect('login.html', null, 'GET');
    });

    $('#balance_inc_id').click(function () {
        $.post({
            url: api.customerBalance(Cookies.get('id')),
            headers: {
                'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass'))
            },
            data: $('#balance_inc_form_id').serialize()
        }).done(function() {
            getCustomerData();
        }).fail(function (jqXHR) {
            alert((jqXHR.responseText.split('\n') || [])[0]);
        });
    });

    $('#buy_subscription_id').click(function () {
        $.redirect('buy_subscription.html', {}, 'GET');
    });
});