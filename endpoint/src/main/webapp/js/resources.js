/**
    * @author Alina Mozhina (mohinalina@gmail.com).
    */
var res = {
    incorrectLoginData: "Email or password is incorrect",
    emptyLoginData: "Email or password is empty"
};