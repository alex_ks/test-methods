/**
    * @author Alina Mozhina (mohinalina@gmail.com).
    */
$(document).ready(function(){
    $("#back_id").click(
        function () {
            $.redirect('admin.html', {}, 'GET');
    });
    $("#create_plan_id").click(
        function() {
            var name = $("#name_id").val();
            var details = $("#details_id").val();
            var minSeats = $("#min_seats_id").val();
            var maxSeats = $("#max_seats_id").val();
            var feePerUnit  = $("#fee_per_unit_id").val();
            var oneTimeCost = $("#one_time_cost_id").val();

            $.post({
                url: api.planCreate,
                headers: {
                    'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass')),
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    "name":name,
                    "details":details,
                    "minSeats":minSeats,
                    "maxSeats":maxSeats,
                    "feePerUnit":feePerUnit,
                    "oneTimeCost":oneTimeCost
                })
            }).done(function(data) {
                $.redirect('admin.html', {}, 'GET');
            }).fail(function(jqXHR) {
                if (jqXHR.status == 400) {
                    $('.field').addClass('broken-field');
                }
                alert((jqXHR.responseText.split('\n') || [])[0]);
            });
        }
    );
});