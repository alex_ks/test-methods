/**
    * @author Alina Mozhina (mohinalina@gmail.com).
    */
var api = {
    root: "rest",
    get customerRoot() { return this.root + "/customers"; },
    get customerList() { return this.root + '/get_customers'; },
    customerData: function (customerId) { return this.customerRoot + "/" + customerId + "/data"; },
    customerBalance: function (customerId) { return this.customerRoot + "/" + customerId + "/balance"; },
    customerSubscriptions: function (customerId) { return this.customerRoot + "/" + customerId + "/subscriptions"; },

    get userRoot() { return this.root + "/users"},
    userList: function(customerId) { return this.customerRoot + "/" + customerId + "/users"; },
    userCreate: function(customerId) { return this.userRoot + "/" + customerId; },

    get planList() { return this.root + "/plans"; },
    get planCreate() { return this.root + "/plans"; },
    planBuy: function(customerId) { return this.customerRoot + "/" + customerId + "/subscriptions"; }
};