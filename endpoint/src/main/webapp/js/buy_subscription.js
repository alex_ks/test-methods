/**
 * @author Alina Mozhina (mohinalina@gmail.com).
 */
$(document).ready(function(){
    function getCustomerData() {
        return $.get({
            url: api.customerData(Cookies.get('id')),
            headers: {
                'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass'))
            }
        }).done(function(customerData) {
            var dataSet = [];
            dataSet.push([customerData.firstName, customerData.lastName, customerData.money]);

            $('#customer_info_id')
                .DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    data: dataSet,
                    columns: [
                        { title: "First Name" },
                        { title: "Last Name" },
                        { title: "Money" }
                    ]
                });
        });
    }

    function getPlanList() {
        return $.get({
            url: api.planList,
            headers: {
                'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass'))
            }
        }).done(function(plans) {
            var dataSet = [];
            for(var i = 0; i < plans.length; i++) {
                var plan = plans[i];
                dataSet.push([plan.Name, plan.Details, plan.MinSeats, plan.MaxSeats,
                    plan.FeePerUnit, plan.OneTimeCost, plan.Id])
            }

            var tableHolder = $('#plan_list_id');
            var table = tableHolder
                .DataTable({
                    destroy: true,
                    data: dataSet,
                    columns: [
                        { title: "Name", name: "name" },
                        { title: "Details" },
                        { title: "Min Seats" },
                        { title: "Max Seats" },
                        { title: "Fee per unit" },
                        { title: "One time cost" },
                        { title: "id", visible: false}
                    ]
                });
            makeSelectable(tableHolder);
        });
    }

    function makeSelectable(tableHolder){
        tableHolder.find('tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                tableHolder.DataTable().$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );
    }

    function getSubscriptionList() {
        return $.get({
            url: api.customerSubscriptions(Cookies.get('id')),
            headers: {
                'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass'))
            }
        }).done(function(subscriptions) {
            var planTable = $('#plan_list_id').DataTable({
                retrieve: true
            });

            var dataSet = [];
            for(var i = 0; i < subscriptions.length; i++) {
                var subscription = subscriptions[i];

                var plan = planTable.rows(function ( idx, data, node ) {
                    return data[6] == subscription.ServicePlanId;
                }).data();

                dataSet.push([subscription.Id, plan[0][0], subscription.UsedSeats, subscription.Status])
            }

            var table = $('#subscription_list_id')
                .DataTable({
                    destroy: true,
                    data: dataSet,
                    columns: [
                        { title: "Id" },
                        { title: "Plan Name" },
                        { title: "Used Seats" },
                        { title: "Status" }
                    ]
                });
        });
    }

    getCustomerData();
    $.when(getPlanList()).done(function() {
        getSubscriptionList();
    });

    $("#back_id").click(function() {
        $.redirect('/endpoint/customer.html', {}, 'GET');
    });

    $('#buy_subscription_id').click( function () {
        var plan = $('#plan_list_id').DataTable().row('.selected').data();
        $.post({
            url: api.planBuy(Cookies.get('id')),
            headers: {
                'Authorization': 'Basic ' + btoa(Cookies.get('username') + ':' + Cookies.get('pass')),
                'Content-Type': 'application/json'
            },
            data: JSON.stringify({
                "planId":plan[6]
            })
        }).done(function(data) {
            getCustomerData();
            getSubscriptionList();
        }).fail(function(jqXHR) {
            alert((jqXHR.responseText.split('\n') || [])[0]);
        });
    } );
});