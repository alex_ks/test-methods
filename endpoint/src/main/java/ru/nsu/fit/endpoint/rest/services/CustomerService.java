package ru.nsu.fit.endpoint.rest.services;

import ru.nsu.fit.endpoint.rest.services.safety.ErrorAutoresponseBase;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Alexander Komissarov on 26.11.16.
 */
@Path("/customers")
public class CustomerService extends ErrorAutoresponseBase {
    public static final String CUSTOMER_ID_PARAM = "customer_id";
    public static final String SUBSCRIPTION_ID_PARAM = "subscription_id";

    @RolesAllowed("ADMIN")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerDataJson) {
        return process(() -> {
            Customer.CustomerData customerData = JsonMapper.fromJson(customerDataJson, Customer.CustomerData.class);
            customerData.validate();
            DBService.createCustomer(customerData);
            return Response.status(Response.Status.OK).entity(customerData.toString()).build();
        });
    }

    @RolesAllowed("ADMIN")
    @DELETE
    @Path("/{" + CUSTOMER_ID_PARAM + "}")
    public Response deleteCustomer(@PathParam(CUSTOMER_ID_PARAM) String customerId) {
        return process(() -> {
            DBService.deleteCustomer(customerId);
            return Response.status(Response.Status.OK).entity(customerId).build();
        });
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{customer_login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        return process(() -> {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            return Response.status(Response.Status.OK).entity("{\"id\":\"" + id.toString() + "\"}").build();
        });
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{" + CUSTOMER_ID_PARAM + "}/data")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerData(@PathParam(CUSTOMER_ID_PARAM) String customerId) {
        return process(() -> Response.status(Response.Status.OK)
                                     .entity(JsonMapper.toJson(DBService.getCustomerDataByID(customerId),
                                             false))
                                     .build());
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{" + CUSTOMER_ID_PARAM + "}/subscriptions")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptions(@PathParam(CUSTOMER_ID_PARAM) String customerId) {
        return process(() -> {
            List<Subscription> subscriptions = DBService.getCustomerSubscriptions(customerId);

            return Response.status(Response.Status.OK)
                    .entity(subscriptions)
                    .build();
        });
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{" + CUSTOMER_ID_PARAM + "}/users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@PathParam(CUSTOMER_ID_PARAM) String customerId) {
        return process(() -> {
            List<User> users = DBService.getCustomerUsers(customerId);

            if (users.size() == 0) {
                return Response.status(Response.Status.OK)
                        .entity("[]")
                        .build();
            }

            StringBuilder builder = new StringBuilder("[");
            builder.append(users.get(0).toString());

            for (int i = 1; i < users.size(); ++i) {
                builder.append(',');
                builder.append(users.get(i).toString());
            }

            builder.append(']');

            return Response.status(Response.Status.OK)
                    .entity(builder.toString())
                    .build();
        });
    }

    public static class PlanData {
        private String planId;

        public String getPlanId() {
            return planId;
        }

        public void setPlanId(String planId) {
            this.planId = planId;
        }
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/{" + CUSTOMER_ID_PARAM + "}/subscriptions")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buyPlan(@PathParam(CUSTOMER_ID_PARAM) String customerId,
                            String planDataJson) {
        return process(() -> {
            PlanData planData = JsonMapper.fromJson(planDataJson, PlanData.class);

            Customer.CustomerData customerData = DBService.getCustomerDataByID(customerId);
            Plan plan = DBService.getPlanById(planData.getPlanId());

            if (DBService.customerHasPlanSubscription(customerId, plan.getId().toString())) {
                return Response.status(Response.Status.FORBIDDEN)
                        .entity("You have already subscribed on this plan")
                        .build();
            }

            if (customerData.getMoney() < plan.getOneTimeCost()) {
                return Response.status(Response.Status.FORBIDDEN)
                        .entity("You do not have enough money to pay the one-time cost")
                        .build();
            }

            String status = Subscription.STATUS_PROVISIONING;
            if (Objects.equals(plan.getType(), Plan.INTERNAL_TYPE)) {
                if (isCompatible(plan)) {
                    status = Subscription.STATUS_DONE;
                }
                else {
                    return Response.status(Response.Status.FORBIDDEN)
                            .entity("You cannot subscribe on this plan " +
                                    "because of existing plans incompatibility")
                            .build();
                }
            }

            Subscription subscription = DBService.subscribeCustomerOnPlan(customerId, plan.getId().toString(), status);

            if (status.equals(Subscription.STATUS_PROVISIONING)) {
                requestExternalConformation(customerData, plan, subscription);
            }

            DBService.updateCustomerBalance(customerId, customerData.getMoney() - plan.getOneTimeCost());

            return Response.status(Response.Status.OK)
                    .entity(subscription.toJson())
                    .build();
        });
    }

    @RolesAllowed("CUSTOMER")
    @HEAD
    @Path("/{" + CUSTOMER_ID_PARAM + "}/subscriptions/{" + SUBSCRIPTION_ID_PARAM + "}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkSubscriptionStatus(@PathParam(CUSTOMER_ID_PARAM) String customerId,
                                            @PathParam(SUBSCRIPTION_ID_PARAM) String subscriptionId) {
        return process(() -> {
            Subscription subscription = DBService.getCustomerSubscription(subscriptionId);
            String status = subscription.getStatus();
            return Response
                    .status(Response.Status.OK)
                    .entity("{\"status\":\"" + status + "\"}")
                    .build();
        });
    }

    private void requestExternalConformation(Customer.CustomerData data, Plan plan, Subscription subscription) {
        ExternalServiceMock.requestConformation(data, plan, subscription);
    }

    private boolean isCompatible(Plan plan) {
        // Checked somehow
        Random random = new Random();
        boolean toBe = random.nextBoolean();
        return toBe || !toBe;
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/{" + CUSTOMER_ID_PARAM + "}/balance")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response topUpBalance(@PathParam(CUSTOMER_ID_PARAM) String customerId,
                                 @FormParam("sum") int sum) {
        return process(() -> {
            Customer.CustomerData customerData = DBService.getCustomerDataByID(customerId);

            DBService.updateCustomerBalance(customerId, customerData.getMoney() + sum);

            return Response.status(Response.Status.OK).build();
        });
    }
}
