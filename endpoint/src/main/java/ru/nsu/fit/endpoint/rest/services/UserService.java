package ru.nsu.fit.endpoint.rest.services;

import ru.nsu.fit.endpoint.rest.services.models.UserModel;
import ru.nsu.fit.endpoint.rest.services.safety.ErrorAutoresponseBase;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static ru.nsu.fit.endpoint.rest.services.CustomerService.CUSTOMER_ID_PARAM;
import static ru.nsu.fit.endpoint.rest.services.CustomerService.SUBSCRIPTION_ID_PARAM;

/**
 * Created by alexander on 03.12.16.
 */
@Path("/users")
public class UserService extends ErrorAutoresponseBase {
    public static final String USER_LOGIN_PARAM = "user_login";
    public static final String USER_ID_PARAM = "user_id";

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/{" + CUSTOMER_ID_PARAM + "}")
    public Response createUser(@PathParam(CUSTOMER_ID_PARAM)String customerId,
                               String userString) {
        return process(() -> {
            UserModel model = UserModel.fromJson(userString);
            User user = new User(UUID.fromString(customerId),
                    model.getFirstName(),
                    model.getLastName(),
                    model.getLogin(),
                    model.getPass(),
                    User.UserRole.fromString(model.getUserRole()));
            DBService.createUser(user);
            return Response.status(Response.Status.OK).entity(user.getId().toString()).build();
        });
    }

    @RolesAllowed("CUSTOMER")
    @DELETE
    @Path("/{" + USER_LOGIN_PARAM + "}")
    public Response deleteUser(@PathParam(USER_LOGIN_PARAM) String userLogin) {
        return process(() -> {
            DBService.deleteUser(userLogin);
            return Response.status(Response.Status.OK).entity(userLogin).build();
        });
    }

    @RolesAllowed("USER")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{" + USER_LOGIN_PARAM + "}")
    public Response login(@PathParam(USER_LOGIN_PARAM) String userLogin) {
        return process(() -> {
            String userId = DBService.getUserIdByLogin(userLogin);
            return Response
                    .status(Response.Status.OK)
                    .entity("{\"id\":\"" + userId + "\"}")
                    .build();
        });
    }

    @RolesAllowed("USER")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{" + USER_ID_PARAM + "}/data")
    public Response getUserData(@PathParam(USER_ID_PARAM) String userId) {
        return process(() -> Response
                .status(Response.Status.OK)
                .entity(UserModel.fromUser(DBService.getUserDataById(userId)))
                .build());
    }

    @RolesAllowed("USER")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{" + USER_ID_PARAM + "}/subscriptions")
    public Response getSubscriptions(@PathParam(USER_ID_PARAM) String userId) {
        return process(() -> {
            List<Subscription> subscriptions = DBService.getUserSubscriptions(userId);
            return Response.status(Response.Status.OK).entity(subscriptions).build();
        });
    }

    @RolesAllowed("USER")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{" + USER_ID_PARAM + "}/role")
    public Response getRoles(@PathParam(USER_ID_PARAM) String userId) {
        return process(() -> Response
                .status(Response.Status.OK)
                .entity("{\"userRole\": \"" + DBService.getUserRoleByID(userId) + "\"}")
                .build());
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/users/{" + USER_LOGIN_PARAM + "}/subscriptions/{" + SUBSCRIPTION_ID_PARAM + "}")
    public Response grantSubscription(@PathParam(USER_LOGIN_PARAM) String userLogin,
                                      @PathParam(SUBSCRIPTION_ID_PARAM) String subscriptionId) {
        return process(() -> {
            Subscription subscription = DBService.getCustomerSubscription(subscriptionId);
            if (!Subscription.STATUS_DONE.equals(subscription.getStatus())) {
                return Response
                        .status(Response.Status.FORBIDDEN)
                        .entity("Cannot activate subscription with status " + subscription.getStatus())
                        .build();
            }
            Plan plan = DBService.getPlanById(subscription.getPlanId().toString());
            if (subscription.getUsedSeats() == plan.getMaxSeats()) {
                return Response
                        .status(Response.Status.FORBIDDEN)
                        .entity("User count on subscription reached its limit")
                        .build();
            }

            String userId = DBService.getUserIdByLogin(userLogin);
            User user = DBService.getUserDataById(userId);

            for (UUID subId : user.getSubscriptionIds()) {
                if (subId.toString().equals(subscriptionId)) {
                    return Response
                            .status(Response.Status.FORBIDDEN)
                            .entity("User already has this subscription")
                            .build();
                }
            }

            Customer.CustomerData data = DBService.getCustomerDataByID(user.getCustomerId().toString());

            if (plan.getFeePerUnit() > data.getMoney()) {
                return Response
                        .status(Response.Status.FORBIDDEN)
                        .entity("You do not have enough money to pay the fee for this user")
                        .build();
            }
            DBService.updateSubscriptionUsedSeats(subscriptionId, subscription.getUsedSeats() + 1);
            DBService.addSubscriptionToUser(userId, subscriptionId);

            return Response
                    .status(Response.Status.OK)
                    .build();
        });
    }

    @RolesAllowed("CUSTOMER")
    @DELETE
    @Path("/users/{" + USER_LOGIN_PARAM + "}/subscriptions/{" + SUBSCRIPTION_ID_PARAM + "}")
    public Response deleteSubscription(@PathParam(USER_LOGIN_PARAM) String userLogin,
                                       @PathParam(SUBSCRIPTION_ID_PARAM) String subscriptionId) {
        return process(() -> {
            Subscription subscription = DBService.getCustomerSubscription(subscriptionId);

            String userId = DBService.getUserIdByLogin(userLogin);
            User user = DBService.getUserDataById(userId);

            boolean hasSubscription = false;

            for (UUID subId : user.getSubscriptionIds()) {
                if (subId.toString().equals(subscriptionId)) {
                    hasSubscription = true;
                    break;
                }
            }

            if (!hasSubscription) {
                return Response
                        .status(Response.Status.FORBIDDEN)
                        .entity("User does not have this subscription")
                        .build();
            }

            DBService.updateSubscriptionUsedSeats(subscriptionId, subscription.getUsedSeats() - 1);
            DBService.deleteSubscriptionFromUser(userId, subscriptionId);

            return Response
                    .status(Response.Status.OK)
                    .build();
        });
    }

    @RolesAllowed("CUSTOMER")
    @PUT
    @Path("/users/{" + USER_LOGIN_PARAM + "}/role}")
    public Response changeRole(@PathParam(USER_LOGIN_PARAM) String userName,
                               @QueryParam("role") String role) {
        return process(() -> {
            DBService.getUserIdByLogin(userName);
            User.UserRole newRole = User.UserRole.fromString(role);
            DBService.setUserRole(userName, newRole);
            return Response.status(Response.Status.OK).build();
        });
    }
}
