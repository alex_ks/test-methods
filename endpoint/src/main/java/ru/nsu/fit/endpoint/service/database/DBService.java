package ru.nsu.fit.endpoint.service.database;

import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.List;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class DBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, money) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String ADD_CUSTOMER_ROLE = "INSERT INTO CUSTOMER_ROLES(customer_id, role) VALUES ('%s', '%s')";
    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_SOMETHING_FROM_CUSTOMER = "select %s from customer where %s";
    private static final String SELECT_CUSTOMER_ROLES = "select role from customer_roles where customer_id='%s'";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";

    private static final Logger logger = LoggerFactory.getLogger("DB_LOG");
    private static final Object generalMutex = new Object();
    private static Connection connection;

    static {
        init();
    }

    public static void createCustomer(Customer.CustomerData customerData) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            Customer customer = new Customer(customerData, UUID.randomUUID());
            try (Statement customerStatement = connection.createStatement();
                 Statement roleStatement = connection.createStatement()) {
                customerStatement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customer.getId(),
                                customer.getData().getFirstName(),
                                customer.getData().getLastName(),
                                customer.getData().getLogin(),
                                customer.getData().getPass(),
                                customer.getData().getMoney()));

                roleStatement.executeUpdate(String.format(ADD_CUSTOMER_ROLE,
                                            customer.getId(),
                                            "CUSTOMER"));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static Customer.CustomerData getCustomerDataByLogin(String customerLogin) {
        return getCustomerDataByID(getCustomerIdByLogin(customerLogin).toString());
    }

    public static Customer.CustomerData getCustomerDataByID(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to get data customer by id");

            try (Statement statement = connection.createStatement()) {

                ResultSet result = statement.executeQuery(String.format(SELECT_SOMETHING_FROM_CUSTOMER,
                        "*",
                        String.format("id='%s'", customerId)));
                Customer.CustomerData customer = null;

                while (result.next()) {
                    if (customer != null) {
                        logger.error("Found more than one user");
                        throw new RuntimeException("Found more than one user with such login");
                    }
                    customer = new Customer.CustomerData(result.getString("first_name"),
                            result.getString("last_name"),
                            result.getString("login"),
                            result.getString("pass"),
                            result.getInt("money"));
                }
                if (customer == null) {
                    throw new IllegalArgumentException("Customer with such id does not exist: " + customerId);
                }

                return customer;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static Set<String> getCustomerRolesByID(UUID id) {
        synchronized (generalMutex) {
            logger.info("Try to get roles login");

            try (Statement statement = connection.createStatement()) {

                ResultSet result = statement.executeQuery(String.format(SELECT_CUSTOMER_ROLES,
                                                                        id.toString()));

                Set<String> roles = new TreeSet<>();

                while (result.next()) {
                    roles.add(result.getString("role"));
                }
                return roles;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<Customer.CustomerData> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Get customers");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<Customer.CustomerData> result = Lists.newArrayList();
                while (rs.next()) {
                    result.add(new Customer.CustomerData(
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getInt(6)));
                }
                return result;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info("Try to get customer id by login");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString("id"));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private static void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://84.237.52.124:3306/test_methods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "test_methods",
                            "Test@NSU");
        } catch (SQLException ex) {
            logger.debug("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.debug("Failed to make connection!");
        }
    }

    private static final String SELECT_CUSTOMER_SUBSCRIPTIONS =
            "select * from subscription where customer_id='%s'";

    public static List<Subscription> getCustomerSubscriptions(String customerId) {
        logger.info("Try to get customer subscriptions by id");

        try (Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(String.format(SELECT_CUSTOMER_SUBSCRIPTIONS,
                                                                    customerId));
            List<Subscription> subscriptions = new ArrayList<>();

            while (result.next()) {
                subscriptions.add(new Subscription(UUID.fromString(result.getString("id")),
                                                    UUID.fromString(result.getString("customer_id")),
                                                    UUID.fromString(result.getString("plan_id")),
                                                    result.getInt("used_seats")));
            }
            return subscriptions;
        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    private static final String SELECT_CUSTOMER_USERS =
            "select * from user where customer_id='%s'";

    public static List<User> getCustomerUsers(String customerId) {
        logger.info("Try to get customer users by id");

        try (Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(String.format(SELECT_CUSTOMER_USERS,
                                                                    customerId));
            List<User> users = new ArrayList<>();

            while (result.next()) {
                String userId = result.getString("id");
                List<UUID> subscriptions = getUserSubscriptionsIds(userId);

                users.add(new User(UUID.fromString(userId),
                                    UUID.fromString(customerId),
                                    subscriptions.toArray(new UUID[subscriptions.size()]),
                                    result.getString("first_name"),
                                    result.getString("last_name"),
                                    result.getString("login"),
                                    result.getString("pass"),
                                    User.UserRole.fromString(result.getString("user_role"))));
            }
            return users;
        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    private static final String SELECT_USER_SUBSCRIPTION_IDS = "select subscription_id from user_assignment where user_id='%s'";
    private static final String SELECT_USER_SUBSCRIPTIONS = "select * from user_assignment where user_id='%s'";

    public static List<UUID> getUserSubscriptionsIds(String userId) {
        logger.info("Try to get user subscriptions ids by id");

        try (Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(String.format(SELECT_USER_SUBSCRIPTION_IDS,
                                                                    userId));
            List<UUID> subscriptions = new ArrayList<>();

            while (result.next()) {
                subscriptions.add(UUID.fromString(result.getString("subscription_id")));
            }
            return subscriptions;
        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    private static Subscription extractSubscription(ResultSet result) throws SQLException {
        return new Subscription(UUID.fromString(result.getString("subscription_id")),
                UUID.fromString(result.getString("customer_id")),
                UUID.fromString(result.getString("plan_id")),
                result.getInt("used_seats"));
    }

    public static List<Subscription> getUserSubscriptions(String userId) {
        logger.info("Try to get user subscriptions ids by id");

        try (Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(String.format(SELECT_USER_SUBSCRIPTIONS,
                    userId));
            List<Subscription> subscriptions = new ArrayList<>();

            while (result.next()) {
                subscriptions.add(extractSubscription(result));
            }
            return subscriptions;
        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    public static final String SELECT_PLAN = "select * from plan where id='%s'";

    private static Plan extractPlan(ResultSet rs) throws SQLException {
        return new Plan(UUID.fromString(rs.getString("id")),
                rs.getString("name"),
                rs.getString("details"),
                rs.getInt("minseats"),
                rs.getInt("maxseats"),
                rs.getInt("fee_per_unit"),
                rs.getInt("one_time_cost"),
                rs.getString("type"));
    }

    public static Plan getPlanById(String planId) {
        synchronized (generalMutex) {
            logger.info("Try to get plan by id");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(SELECT_PLAN,
                                      planId));
                if (rs.next()) {
                    return extractPlan(rs);
                } else {
                    throw new IllegalArgumentException("Plan with id '" + planId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String CHECK_SUBSCRIPTION_EXISTANCE = "select * from subscription where customer_id='%s' and plan_id='%s'";

    public static boolean customerHasPlanSubscription(String customerId, String planId) {
        synchronized (generalMutex) {
            logger.info("Try to get plan by id");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(CHECK_SUBSCRIPTION_EXISTANCE,
                                customerId,
                                planId));
                return rs.next();
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String ADD_SUBSCRIPTION = "insert into subscription(id, customer_id, plan_id, used_seats) " +
            "values('%s', '%s', '%s', 0)";

    public static Subscription subscribeCustomerOnPlan(String customerId, String planId, String status) {
        synchronized (generalMutex) {
            logger.info("Try to create subscription for customer");

            String id = UUID.randomUUID().toString();

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(
                                ADD_SUBSCRIPTION,
                                id,
                                customerId,
                                planId));
                return new Subscription(UUID.fromString(id), UUID.fromString(customerId), UUID.fromString(planId), 0);
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String UPDATE_CUSTOMER_BALANCE = "update customer set money = %d where id = '%s'";

    public static void updateCustomerBalance(String customerId, int balance) {
        synchronized (generalMutex) {
            logger.info("Try to update customer balance");

            String id = UUID.randomUUID().toString();

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(UPDATE_CUSTOMER_BALANCE,
                                      balance,
                                      customerId));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String DELETE_CUSTOMER_BY_ID = "delete from customer where id = '%s'";

    public static void deleteCustomer(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to delete customer " + customerId);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(DELETE_CUSTOMER_BY_ID,
                                      customerId));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private static final String CREATE_PLAN = "insert into " +
            "plan(id, name, details, minseats, maxseats, fee_per_unit, one_time_cost, type) " +
            "values('%s', '%s', '%s', '%d', '%d', '%d', '%d', '%s')";

    public static void createPlan(Plan plan) {
        synchronized (generalMutex) {
            logger.info("Try to create plan " + plan.getName());

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(CREATE_PLAN,
                                    plan.getId(),
                                    plan.getName(),
                                    plan.getDetails(),
                                    plan.getMinSeats(),
                                    plan.getMaxSeats(),
                                    plan.getFeePerUnit(),
                                    plan.getOneTimeCost(),
                                    plan.getType()));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String SELECT_ALL_PLANS = "select * from plan";

    public static List<Plan> getAllPlans() {
        synchronized (generalMutex) {
            logger.info("Try to get all available plans");

            List<Plan> resultList = new ArrayList<>();

            try (Statement statement = connection.createStatement()) {
                ResultSet res = statement.executeQuery(SELECT_ALL_PLANS);

                while (res.next()) {
                    resultList.add(extractPlan(res));
                }
                return resultList;

            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private static final String CREATE_USER = "insert into " +
            "user(id, customer_id, first_name, last_name, login, pass, user_role) " +
            "values('%s', '%s', '%s', '%s', '%s', '%s', '%s')";

    public static void createUser(User user) {
        synchronized (generalMutex) {
            logger.info("Try to create user " + user.getFirstName() + " " + user.getLastName());

            try (Statement statement = connection.createStatement();
                 Statement roleStatement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(CREATE_USER,
                                user.getId(),
                                user.getCustomerId(),
                                user.getFirstName(),
                                user.getLastName(),
                                user.getLogin(),
                                user.getPass(),
                                user.getUserRole().getRoleName()));
                roleStatement.executeUpdate(String.format(ADD_CUSTOMER_ROLE,
                                            user.getId(),
                                            "USER"));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String DELETE_USER_BY_LOGIN = "delete from user where login = '%s'";

    public static void deleteUser(String userLogin) {
        synchronized (generalMutex) {
            logger.info("Try to delete user " + userLogin);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(DELETE_USER_BY_LOGIN,
                                      userLogin));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String SELECT_USER_ID = "select id from user where login='%s'";

    public static String getUserIdByLogin(String userLogin) {
        synchronized (generalMutex) {
            logger.info("Try to get user id by login " + userLogin);

            try(Statement statement = connection.createStatement()) {
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_ID,
                                userLogin));
                if (rs.next()) {
                    return rs.getString("id");
                } else {
                    String msg = "User with login '" + userLogin + " was not found";
                    logger.error(msg);
                    throw new IllegalArgumentException(msg);
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String GET_USER_DATA =
            "select * from user where id = '%s'";

    public static User getUserDataById(String userId) {
        synchronized (generalMutex) {
            logger.info("Try to get user data by id, no subscriptions counted");

            try (Statement statement = connection.createStatement()) {

                ResultSet result = statement
                        .executeQuery(String.format(GET_USER_DATA,
                                                    userId));

                List<UUID> subIds = DBService.getUserSubscriptionsIds(userId);
                UUID[] subscriptions = new UUID[subIds.size()];
                subIds.toArray(subscriptions);

                User user;
                if (result.next()) {
                    user = new User(UUID.fromString(result.getString("id")),
                            UUID.fromString(result.getString("customer_id")),
                            subscriptions,
                            result.getString("first_name"),
                            result.getString("last_name"),
                            result.getString("login"),
                            result.getString("pass"),
                            User.UserRole.fromString(result.getString("user_role")));
                }
                else {
                    String msg = "User with such id does not exist: " + userId;
                    throw new IllegalArgumentException(msg);
                }

                return user;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String GET_SUBSCRIPTION = "select * from subscription where id='%s'";

    public static Subscription getCustomerSubscription(String subscriptionId) {
        logger.info("Try to get customer subscription by id");

        try (Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(String.format(GET_SUBSCRIPTION,
                                                                    subscriptionId));

            if (result.next()) {
                return extractSubscription(result);
            }
            else {
                String msg = "Subscription " + subscriptionId + " does not exist";
                logger.error(msg);
                throw new IllegalArgumentException(msg);
            }
        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    public static final String SET_SUBSCRIPTION_STATUS = "update subscription set status = '%s' where id = '%s'";

    public static void changeSubscriptionStatus(String subscriptionId, String status) {
        synchronized (generalMutex) {
            logger.info("Try to change status of subscription " + subscriptionId);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(SET_SUBSCRIPTION_STATUS,
                                      status,
                                      subscriptionId));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String SET_USED_SEATS = "update subscription set used_seats = %d where id = '%s'";

    public static void updateSubscriptionUsedSeats(String subscriptionId, int newCount) {
        synchronized (generalMutex) {
            logger.info("Try to update used seats count of subscription " + subscriptionId);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(SET_USED_SEATS,
                                newCount,
                                subscriptionId));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String ADD_SUBSCRIPTION_TO_USER = "insert into user_assignment(user_id, subscription_id) values('%s', '%s')";

    public static void addSubscriptionToUser(String userId, String subscriptionId) {
        synchronized (generalMutex) {
            logger.info("Try to add subscription " + subscriptionId + " to user " + userId);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(ADD_SUBSCRIPTION_TO_USER,
                                      userId,
                                      subscriptionId));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String DELETE_SUBSCRIPTION_FROM_USER =
            "delete from user_assignment where user_id = '%s' and subscription_id = '%s'";

    public static void deleteSubscriptionFromUser(String userId, String subscriptionId) {
        synchronized (generalMutex) {
            logger.info("Try to remove subscription " + subscriptionId + " from user " + userId);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(DELETE_SUBSCRIPTION_FROM_USER,
                                userId,
                                subscriptionId));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String CHANGE_ROLE = "update user set user_role = '%s' where login = '%s'";

    public static void setUserRole(String userName, User.UserRole newRole) {
        synchronized (generalMutex) {
            logger.info("Try to change role of user " + userName + " to " + newRole);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(CHANGE_ROLE,
                                newRole,
                                userName));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static final String GET_USER_ROLE = "select user_role from user where id = '%s'";

    public static String getUserRoleByID(String userId) {
        logger.info("Try to get customer subscription by id");

        try (Statement statement = connection.createStatement()) {

            ResultSet result = statement.executeQuery(String.format(GET_USER_ROLE,
                                                                    userId));
            if (result.next()) {
                return result.getString("user_role");
            }
            else {
                String msg = "User " + userId + " does not exist";
                logger.error(msg);
                throw new IllegalArgumentException(msg);
            }
        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    public static final String DELETE_PLAN_BY_NAME =
            "delete from plan where name = '%s'";

    public static void deletePlanByName(String name) {
        synchronized (generalMutex) {
            logger.info("Try to remove plan " + name + " from user ");

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        String.format(DELETE_PLAN_BY_NAME,
                                      name));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
}
