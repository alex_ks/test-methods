package ru.nsu.fit.endpoint.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.management.relation.Role;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Random;
import java.util.List;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Path("")
public class RestService {
    @PermitAll
    @GET
    @Path("/health_check")
    public Response healthCheck() {
        return Response.ok().entity("{\"status\": \"OK\"}").build();
    }

    @RolesAllowed({Roles.ADMIN, Roles.CUSTOMER, Roles.USER})
    @GET
    @Path("/get_role")
    public Response getRole(@Context ContainerRequestContext crc) {
        return Response.ok().entity(String.format("{\"role\": \"%s\"}", crc.getProperty("ROLE"))).build();
    }

    @RolesAllowed(Roles.ADMIN)
    @GET
    @Path("/get_customers")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCustomers() {
        try {
            List<Customer.CustomerData> result = DBService.getCustomers();
            String resultData = JsonMapper.toJson(result, true);
            return Response.ok().entity(resultData).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Roles.ADMIN)
    @GET
    @Path("/get_customer_id/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            return Response.ok().entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @GET
    @Path("/magic_number")
    @PermitAll
    public Response getMagicNumber(/*@PathParam("limit")String limit*/) {
        try {
            int n = 25;//Integer.parseInt(limit);
            Random random = new Random();
            return Response.status(200).entity(random.nextInt(n)).build();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    static class MagicData {
        @JsonProperty("magicNumber")
        private int magicNumber;

        @JsonProperty("magicString")
        private String magicString;

        public int getMagicNumber() {
            return magicNumber;
        }

        public void setMagicNumber(int magicNumber) {
            this.magicNumber = magicNumber;
        }

        public String getMagicString() {
            return magicString;
        }

        public void setMagicString(String magicString) {
            this.magicString = magicString;
        }
    }

    @POST
    @Path("/magic_data")
    @PermitAll
    public Response setMagicData(String dataString) {
        try {
            MagicData data = JsonMapper.fromJson(dataString, MagicData.class);
            return Response.status(200).entity(data.getMagicString() + data.getMagicNumber()).build();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).build();
        }
    }
}