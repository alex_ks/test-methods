package ru.nsu.fit.endpoint.service.database.data;

import org.apache.commons.lang.Validate;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Plan {
    private UUID id;
    /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
    private String name;
    /* Длина не больше 1024 символов и не меньше 1 включительно */
    private String details;
    /* Не больше 999999 и не меньше 1 включительно */
    private int maxSeats;
    /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
    private int minSeats;
    /* Больше ли равно 0 но меньше либо равно 999999 */
    private int feePerUnit;
    /* Больше ли равно 0 но меньше либо равно 999999 */
    private int oneTimeCost;

    /* INTERNAL или EXTERNAL */
    private String type;

    public static final String INTERNAL_TYPE = "INTERNAL";
    public static final String EXTERNAL_TYPE = "EXTERNAL";

    public Plan(UUID id, String name, String details, int minSeats, int maxSeats, int feePerUnit, int oneTimeCost, String type) {
        validate(name, details, minSeats, maxSeats, feePerUnit, oneTimeCost);
        this.id = id;
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.feePerUnit = feePerUnit;
        this.oneTimeCost = oneTimeCost;
        this.type = type;
    }

    public Plan(String name, String details, int minSeats, int maxSeats, int feePerUnit, int oneTimeCost, String type) {
        this(UUID.randomUUID(), name, details, minSeats, maxSeats, feePerUnit, oneTimeCost, type);
    }

    public Plan(String name, String details, int minSeats, int maxSeats, int feePerUnit, int oneTimeCost) {
        this(UUID.randomUUID(), name, details, minSeats, maxSeats, feePerUnit, oneTimeCost, INTERNAL_TYPE);
    }

    public Plan(String name, String details, int minSeats, int maxSeats, int feePerUnit) {
        this(name, details, minSeats, maxSeats, feePerUnit, 10);
    }

    private static Pattern wrongNamePattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);

    private static void validate(String name, String details, int minSeats, int maxSeats, int feePerUnit, int oneTimeCost) {
        if (name == null) {
            throw new IllegalArgumentException(DataResources.SERVICE_PLAN_NAME_IS_NULL);
        }

        if (details == null) {
            throw new IllegalArgumentException(DataResources.SERVICE_PLAN_DETAILS_ARE_NULL);
        }

        if (!(name.length() > 2 && name.length() <= 128)) {
            throw new IllegalArgumentException(DataResources.INVALID_SERVICE_PLAN_NAME_LENGTH);
        }

        if (!(details.length() > 1 && details.length() <= 1024)) {
            throw new IllegalArgumentException(DataResources.INVALID_SERVICE_PLAN_DETAILS_LENGTH);
        }

        if (wrongNamePattern.matcher(name).find()) {
            throw new IllegalArgumentException(DataResources.INVALID_SERVICE_PLAN_NAME_SYMBOLS);
        }

        if (!(minSeats >= 1 && minSeats < 1e6)) {
            throw new IllegalArgumentException(DataResources.MIN_SEATS_VALUE_OUT_OF_RANGE);
        }

        if (!(maxSeats >= minSeats && maxSeats < 1e6)) {
            throw new IllegalArgumentException(DataResources.MAX_SEATS_VALUE_OUT_OF_RANGE);
        }

        if (!(feePerUnit >= 0 && feePerUnit <= 1e6)) {
            throw new IllegalArgumentException(DataResources.FEE_PER_UNIT_OUT_OF_RANGE);
        }

        if (!(oneTimeCost >= 0 && oneTimeCost <= 1e6)) {
            throw new IllegalArgumentException(DataResources.ONE_TIME_COST_OUT_OF_RANGE);
        }
    }

    public int getOneTimeCost() {
        return oneTimeCost;
    }

    public int getFeePerUnit() {
        return feePerUnit;
    }

    public int getMinSeats() {
        return minSeats;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public String getDetails() {
        return details;
    }

    public String getName() {
        return name;
    }

    public UUID getId() {
        return id;
    }

    public String getType() {
        return type;
    }
}
