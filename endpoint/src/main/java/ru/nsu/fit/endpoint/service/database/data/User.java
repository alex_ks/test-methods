package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.Validate;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    private UUID customerId;
    private UUID[] subscriptionIds;
    private UUID id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    private UserRole userRole;

    public User(UUID id, UUID customerId, UUID[] subscriptionIds, String firstName, String lastName, String login, String pass, UserRole userRole) {
        validate(firstName, lastName, login, pass);
        this.id = id;
        this.subscriptionIds = subscriptionIds;
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
        this.userRole = userRole;
    }

    public User(UUID customerId, String firstName, String lastName, String login, String pass, UserRole userRole) {
        this(UUID.randomUUID(), customerId, null, firstName, lastName, login, pass, userRole);
    }

    private static Pattern namePattern = Pattern.compile("^[A-Z]([a-z]+)$");
    private static Pattern emailPattern = Pattern.compile("^([a-z0-9_\\.-]+)@([a-z0-9_\\.-]+)\\.([a-z\\.]{2,6})$", Pattern.CASE_INSENSITIVE);

    private static void validate(String firstName, String lastName, String login, String pass) {
        if (pass == null) {
            throw new IllegalArgumentException(DataResources.PASSWORD_IS_NULL);
        }

        if (!(pass.length() >= 6 && pass.length() < 13)) {
            throw new IllegalArgumentException(DataResources.INVALID_PASSWORD_LENGTH);
        }

        if (pass.equalsIgnoreCase("123qwe")) {
            throw new IllegalArgumentException(DataResources.EASY_PASSWORD);
        }

        if (!namePattern.matcher(firstName).matches()) {
            throw new IllegalArgumentException(DataResources.INVALID_FIRST_NAME);
        }

        if (!namePattern.matcher(lastName).matches()) {
            throw new IllegalArgumentException(DataResources.INVALID_LAST_NAME);
        }

        if (!emailPattern.matcher(login).matches()) {
            throw new IllegalArgumentException(DataResources.INVALID_EMAIL);
        }

        if (pass.contains(firstName) || pass.contains(lastName)) {
            throw new IllegalArgumentException(DataResources.NAME_DEPENDENT_PASSWORD);
        }
    }

    public UUID getId() {
        return id;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public UUID[] getSubscriptionIds() {
        return subscriptionIds;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public static enum UserRole {
        COMPANY_ADMINISTRATOR(DataResources.COMPANY_ADMIN_ROLE),
        TECHNICAL_ADMINISTRATOR(DataResources.TECHNICAL_ADMIN_ROLE),
        BILLING_ADMINISTRATOR(DataResources.BILLING_ADMIN_ROLE),
        USER(DataResources.USER_ROLE);

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public static UserRole fromString(String string) {
            switch (string) {
                case DataResources.COMPANY_ADMIN_ROLE:
                    return COMPANY_ADMINISTRATOR;
                case DataResources.TECHNICAL_ADMIN_ROLE:
                    return TECHNICAL_ADMINISTRATOR;
                case DataResources.BILLING_ADMIN_ROLE:
                    return BILLING_ADMINISTRATOR;
                case DataResources.USER_ROLE:
                    return USER;
                default:
                    throw new RuntimeException("No such role found: " + string);
            }
        }

        public String getRoleName() {
            return roleName;
        }
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append('{')
                .append("\"id\": \"")
                .append(id.toString())
                .append("\", \"customerId\":\"")
                .append(customerId.toString())
                .append("\", \"subscriptionIds\":[");

        if (subscriptionIds != null && subscriptionIds.length != 0) {
            builder.append('"')
                    .append(subscriptionIds[0].toString())
                    .append('"');
            for (int i = 1; i < subscriptionIds.length; ++i) {
                builder.append(",\"")
                        .append(subscriptionIds[i].toString())
                        .append('"');
            }
        }

        builder.append(']');

        builder.append(", \"firstName\":\"")
                .append(firstName)
                .append("\", \"lastName\":\"")
                .append(lastName)
                .append("\", \"login\":\"")
                .append(login)
                .append("\", \"userRole\":\"")
                .append(userRole.getRoleName())
                .append("\"}");
        return builder.toString();
    }
}
