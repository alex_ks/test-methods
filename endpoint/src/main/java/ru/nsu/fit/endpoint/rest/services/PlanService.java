package ru.nsu.fit.endpoint.rest.services;

import org.apache.commons.lang.NotImplementedException;
import ru.nsu.fit.endpoint.rest.services.models.PlanModel;
import ru.nsu.fit.endpoint.rest.services.safety.ErrorAutoresponseBase;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alexander on 02.12.16.
 */
@Path("/plans")
public class PlanService extends ErrorAutoresponseBase {
    private static final String PLAN_NAME_PARAM = "planName";

    @RolesAllowed("ADMIN")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPlan(String planString) {
        return process(() -> {
            PlanModel model = PlanModel.fromJson(planString);
            String type = model.isExternal() ? Plan.EXTERNAL_TYPE : Plan.INTERNAL_TYPE;
            Plan plan = new Plan(model.getName(),
                                 model.getDetails(),
                                 model.getMinSeats(),
                                 model.getMaxSeats(),
                                 model.getFeePerUnit(),
                                 model.getOneTimeCost(),
                                 type);
            DBService.createPlan(plan);
            return Response.status(Response.Status.OK)
                           .entity(plan.getId().toString())
                           .build();
        });
    }

    @RolesAllowed({"CUSTOMER", "ADMIN"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlans() {
        return process(() -> {
            List<Plan> allPlans = DBService.getAllPlans();
            List<PlanModel> result = allPlans
                    .stream()
                    .map(PlanModel::fromPlan)
                    .collect(Collectors.toList());

            return Response
                    .status(Response.Status.OK)
                    .entity(result)
                    .build();
        });
    }

    @RolesAllowed("ADMIN")
    @DELETE
    @Path("/{" + PLAN_NAME_PARAM + "}")
    public Response deletePlan(@QueryParam(PLAN_NAME_PARAM) String name) {
        return process(() -> {
            DBService.deletePlanByName(name);
            return Response
                    .status(Response.Status.OK)
                    .entity(name)
                    .build();
        });
    }
}
