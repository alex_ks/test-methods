package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.Validate;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import java.io.IOException;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription {
    @JsonProperty
    private UUID id;
    @JsonProperty
    private UUID customerId;
    @JsonProperty
    private UUID servicePlanId;
    @JsonProperty
    private int usedSeats;
    @JsonProperty
    private String status;

    public static final String STATUS_PROVISIONING = "PROVISIONING";
    public static final String STATUS_DONE = "DONE";
    public static final String STATUS_REJECTED = "REJECTED";

    public Subscription(UUID id, UUID customerId, UUID servicePlanId, int usedSeats, String status) {
        this.id = id;
        this.customerId = customerId;
        this.servicePlanId = servicePlanId;
        this.usedSeats = usedSeats;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public Subscription(UUID id, UUID customerId, UUID servicePlanId, int usedSeats) {
        this(id, customerId, servicePlanId, usedSeats, STATUS_DONE);
    }

    private static void validate(int minSeats, int maxSeats, int usedSeats) {
        if (!(minSeats >= 1 && minSeats < 1e6)) {
            throw new IllegalArgumentException(DataResources.MIN_SEATS_VALUE_OUT_OF_RANGE);
        }

        if (!(maxSeats >= minSeats && maxSeats <= 1e6)) {
            throw new IllegalArgumentException(DataResources.MAX_SEATS_VALUE_OUT_OF_RANGE);
        }

        if (!(usedSeats >= minSeats && usedSeats <= maxSeats)) {
            throw new IllegalArgumentException(DataResources.USED_SEATS_VALUE_OUT_OF_RANGE);
        }
    }

    public String toJson() throws IOException {
        return JsonMapper.toJson(this, true);
    }

    @Override
    @Deprecated
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append('{')
                .append("\"id\": \"")
                .append(id.toString())
                .append("\", \"customerId\":\"")
                .append(customerId.toString())
                .append("\", \"planId\":\"")
                .append(servicePlanId.toString())
                .append("\", \"usedSeats\":")
                .append(usedSeats)
                .append("\", \"status\":")
                .append(status)
                .append('}');
        return builder.toString();
    }

    public UUID getId() {
        return id;
    }

    public UUID getPlanId() {
        return servicePlanId;
    }

    public int getUsedSeats() {
        return usedSeats;
    }
}
