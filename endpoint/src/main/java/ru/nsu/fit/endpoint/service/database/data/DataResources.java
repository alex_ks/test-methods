package ru.nsu.fit.endpoint.service.database.data;

/**
 * Created by alexander on 03.10.16.
 */
public class DataResources {
    public static final String INVALID_PASSWORD_LENGTH = "Password's length should be more or equal 6 symbols and less or equal 12 symbols";
    public static final String EASY_PASSWORD = "Password is easy";
    public static final String NEGATIVE_MONEY = "Money must be positive value";
    public static final String INVALID_FIRST_NAME = "First name must start with capital letter and contain only letters";
    public static final String INVALID_LAST_NAME = "Last name must start with capital letter and contain only letters";
    public static final String INVALID_EMAIL = "Invalid email format";
    public static final String NAME_DEPENDENT_PASSWORD = "Password should not contain first or last name";
    public static final String COMPANY_ADMIN_ROLE = "Company administrator";

    public static final String TECHNICAL_ADMIN_ROLE = "Technical administrator";
    public static final String BILLING_ADMIN_ROLE = "Billing administrator";
    public static final String USER_ROLE = "User";

    public static final String INVALID_SERVICE_PLAN_NAME_LENGTH = "Service plan name must be more or equal 2 symbols and less or equal 128 symbols";
    public static final String INVALID_SERVICE_PLAN_DETAILS_LENGTH = "Service plan details must be more or equal 1 symbols and less or equal 1024 symbols";
    public static final String INVALID_SERVICE_PLAN_NAME_SYMBOLS = "Service plan name must contain only letters or numbers";
    public static final String MIN_SEATS_VALUE_OUT_OF_RANGE = "Seats count minimum must be more or equal 1 and less or equal 999999";
    public static final String MAX_SEATS_VALUE_OUT_OF_RANGE = "Seats count maximum must be more or equal seats count minimum and less or equal 999999";
    public static final String FEE_PER_UNIT_OUT_OF_RANGE = "Fee per unit count must be more or equal 0 and less or equal 999999";
    public static final String SERVICE_PLAN_NAME_IS_NULL = "Service plan name is null";
    public static final String SERVICE_PLAN_DETAILS_ARE_NULL = "Service plan details are null";
    public static final String PASSWORD_IS_NULL = "Password is null";
    public static final String USED_SEATS_VALUE_OUT_OF_RANGE = "Used seats count must be more or equal seats count minimum and less or equal seats count maximum";
    public static final String ONE_TIME_COST_OUT_OF_RANGE = "One-time count must be more or equal 0 and less or equal 999999";
}
