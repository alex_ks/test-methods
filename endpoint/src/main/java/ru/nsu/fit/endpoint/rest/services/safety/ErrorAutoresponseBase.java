package ru.nsu.fit.endpoint.rest.services.safety;

import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.ws.rs.core.Response;

/**
 * Created by alexander on 02.12.16.
 */
public abstract class ErrorAutoresponseBase {
    protected Response process(HttpResponseMaker responseMaker) {
        try {
            return responseMaker.createResponse();
        }
        catch (Exception e) {
            return Response.status(400).entity(e.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(e)).build();
        }
    }
}
