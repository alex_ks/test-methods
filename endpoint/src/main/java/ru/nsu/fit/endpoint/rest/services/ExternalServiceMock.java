package ru.nsu.fit.endpoint.rest.services;

import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.util.concurrent.Executors;

/**
 * Created by alexander on 04.12.16.
 */
public class ExternalServiceMock {
    public static void requestConformation(Customer.CustomerData data, Plan plan, Subscription subscription) {
        Executors.newCachedThreadPool().submit(() -> {
            try {
                Thread.sleep(200);
                DBService.changeSubscriptionStatus(subscription.getId().toString(), Subscription.STATUS_DONE);
            }
            catch (Exception e) {
                System.err.println(e.getMessage());
            }
        });
    }
}
