package ru.nsu.fit.endpoint.rest.services.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import java.io.IOException;

/**
 * Created by alexander on 03.12.16.
 */
public class UserModel {
    @JsonProperty
    private String firstName;
    @JsonProperty
    private String lastName;
    @JsonProperty
    private String login;
    @JsonProperty("password")
    private String pass;
    @JsonProperty
    private String userRole;

    public static UserModel fromJson(String json) {
        return JsonMapper.fromJson(json, UserModel.class);
    }

    public String toJson() throws IOException {
        return JsonMapper.toJson(this, true);
    }

    public static UserModel fromUser(User user) {
        UserModel model = new UserModel();
        model.setFirstName(user.getFirstName());
        model.setLastName(user.getLastName());
        model.setLogin(user.getLastName());
        model.setPass(user.getPass());
        model.setUserRole(user.getUserRole().getRoleName());
        return model;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
