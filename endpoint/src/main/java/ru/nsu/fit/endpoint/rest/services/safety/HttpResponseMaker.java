package ru.nsu.fit.endpoint.rest.services.safety;

import javax.ws.rs.core.Response;

/**
 * Created by alexander on 02.12.16.
 */
@FunctionalInterface
public interface HttpResponseMaker {
    public Response createResponse() throws Exception;
}
