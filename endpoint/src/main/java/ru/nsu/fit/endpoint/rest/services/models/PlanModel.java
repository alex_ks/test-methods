package ru.nsu.fit.endpoint.rest.services.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by alexander on 02.12.16.
 */
public class PlanModel {
    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("details")
    private String details;

    @JsonProperty("maxSeats")
    private int maxSeats;

    @JsonProperty("minSeats")
    private int minSeats;

    @JsonProperty("feePerUnit")
    private int feePerUnit;

    @JsonProperty("oneTimeCost")
    private int oneTimeCost;

    @JsonProperty
    private boolean isExternal = false;

    public static PlanModel fromPlan(Plan plan) {
        PlanModel model = new PlanModel();
        model.setId(plan.getId().toString());
        model.setName(plan.getName());
        model.setDetails(plan.getDetails());
        model.setMinSeats(plan.getMinSeats());
        model.setMaxSeats(plan.getMaxSeats());
        model.setFeePerUnit(plan.getFeePerUnit());
        model.setOneTimeCost(plan.getOneTimeCost());
        model.setExternal(Plan.EXTERNAL_TYPE.equals(plan.getType()));
        return model;
    }

    public static PlanModel fromJson(String json) {
        return JsonMapper.fromJson(json, PlanModel.class);
    }

    public String toJson() throws IOException {
        return JsonMapper.toJson(this, true);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }

    public int getMinSeats() {
        return minSeats;
    }

    public void setMinSeats(int minSeats) {
        this.minSeats = minSeats;
    }

    public int getFeePerUnit() {
        return feePerUnit;
    }

    public void setFeePerUnit(int feePerUnit) {
        this.feePerUnit = feePerUnit;
    }

    public int getOneTimeCost() {
        return oneTimeCost;
    }

    public void setOneTimeCost(int oneTimeCost) {
        this.oneTimeCost = oneTimeCost;
    }

    public boolean isExternal() {
        return isExternal;
    }

    public void setExternal(boolean external) {
        isExternal = external;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
