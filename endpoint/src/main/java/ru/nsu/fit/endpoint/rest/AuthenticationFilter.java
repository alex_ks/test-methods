package ru.nsu.fit.endpoint.rest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.management.relation.Role;
import javax.ws.rs.PathParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.internal.util.Base64;
import ru.nsu.fit.endpoint.rest.services.CustomerService;
import ru.nsu.fit.endpoint.rest.services.UserService;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.Globals;


/**
 * This filter verify the access permissions for a user
 * based on username and password provided in request
 *
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Provider
public class AuthenticationFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME = "Basic";

    @Override
    public void filter(ContainerRequestContext requestContext) {
        Method method = resourceInfo.getResourceMethod();
        //Access allowed for all
        if (!method.isAnnotationPresent(PermitAll.class)) {
            //Access denied for all
            if (method.isAnnotationPresent(DenyAll.class)) {
                Response ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN).entity("Access blocked for all users !!").build();
                requestContext.abortWith(ACCESS_FORBIDDEN);
                return;
            }

            //Get request headers
            final MultivaluedMap<String, String> headers = requestContext.getHeaders();

            //Fetch authorization header
            final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);

            //If no authorization information present; block access
            if (authorization == null || authorization.isEmpty()) {
                Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED).entity("You cannot access this resource").build();
                requestContext.abortWith(ACCESS_DENIED);
                return;
            }

            //Get encoded username and password
            final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");

            //Decode username and password
            String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));

            //Split username and password tokens
            final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken();

            //Verifying Username and password
            System.out.println("User: " + username);
            System.out.println("Pass: " + password);

            //Verify user access
            if (method.isAnnotationPresent(RolesAllowed.class)) {
                RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
                Set<String> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));

                //Is user valid?
                if (!isUserAllowed(username, password, rolesSet, requestContext)) {
                    Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED).entity("You cannot access this resource").build();
                    requestContext.abortWith(ACCESS_DENIED);
                    return;
                }

                //If customer sent wrong id; block
                if ((rolesSet.contains(Roles.CUSTOMER) && !isRealCustomerIdParam(username, requestContext))
                        || (rolesSet.contains(Roles.USER) && !isRealUserIdParam(username, requestContext))) {
                    Response wrongId = Response.status(Response.Status.FORBIDDEN)
                            .entity("You cannot use this id")
                            .build();
                    requestContext.abortWith(wrongId);
                }

                if (rolesSet.contains(Roles.CUSTOMER) && !isRealCustomerUserParam(username, requestContext)) {
                    Response noSuchUser = Response.status(Response.Status.FORBIDDEN)
                            .entity("You do not have user you try to access")
                            .build();
                    requestContext.abortWith(noSuchUser);
                }
            }
        }
    }

    private boolean isUserAllowed(final String username, final String password, final Set<String> rolesSet, ContainerRequestContext requestContext) {
        //Step 1. Fetch password from database and match with password in argument
        //If both match then get the defined role for user from database and continue; else return isAllowed [false]
        //Access the database and do this part yourself
        //String userRole = userMgr.getUserRole(username);

        if (username.equals(Globals.ADMIN_LOGIN) && password.equals(Globals.ADMIN_PASS)) {
            String userRole = Roles.ADMIN;

            //Step 2. Verify user role
            if (rolesSet.contains(userRole)) {
                requestContext.setProperty("ROLE", userRole);
                return true;
            }
        }

        String id = null;

        if (rolesSet.contains(Roles.CUSTOMER)) {
            try {
                id = DBService.getCustomerIdByLogin(username).toString();
                Customer.CustomerData data = DBService.getCustomerDataByID(id);
                if (!data.getPass().equals(password)) {
                    return false;
                }
            }
            catch (IllegalArgumentException ignored) {
                return false;
            }
        }
        else if (rolesSet.contains(Roles.USER)) {
            try {
                id = DBService.getUserIdByLogin(username);
                User user = DBService.getUserDataById(id);
                if (!user.getPass().equals(password)) {
                    return false;
                }
            }
            catch (IllegalArgumentException ignored) {
                return false;
            }
        }
        else {
            throw new UnknownError("UNKNOWN ROLE");
        }

        Set<String> customerRoles = DBService.getCustomerRolesByID(UUID.fromString(id));

        for (String role : customerRoles) {
            requestContext.setProperty("ROLE", role);
            if (rolesSet.contains(role)) {
                return true;
            }
        }

        return false;
    }

    //Check if request has "id" parameter and if it matches real user id
    private boolean isRealCustomerIdParam(String username, ContainerRequestContext context) {
        return isRealSomeoneIdParam(username,
                                    CustomerService.CUSTOMER_ID_PARAM,
                                    (login) -> DBService.getCustomerIdByLogin(login).toString(),
                                    context);
    }

    private boolean isRealUserIdParam(String username, ContainerRequestContext context) {
        return isRealSomeoneIdParam(username,
                UserService.USER_ID_PARAM,
                DBService::getUserIdByLogin,
                context);
    }

    @FunctionalInterface
    interface IdGetter {
        String getIdByLogin(String login);
    }

    private List<String> getPathParameterValues(String paramName, ContainerRequestContext context) {
        Method method = resourceInfo.getResourceMethod();

        boolean idParamFound = false;
        for (Annotation[] annos : method.getParameterAnnotations()) {
            for (Annotation anno : annos) {
                if (anno instanceof PathParam
                        && ((PathParam)anno).value().equals(paramName)) {
                    idParamFound = true;
                    break;
                }
            }
        }

        if (!idParamFound) {
            return null;
        }

        MultivaluedMap<String, String> pathParameters = context.getUriInfo()
                .getPathParameters();

        if (!pathParameters.containsKey(paramName)) {
            return null;
        }

        return pathParameters.get(paramName);
    }

    private boolean isRealSomeoneIdParam(String username,
                                         String paramName,
                                         IdGetter idGetter,
                                         ContainerRequestContext context) {
        List<String> values = getPathParameterValues(paramName, context);

        if (values == null) {
            return true;
        }

        String id = idGetter.getIdByLogin(username);

        for (String value : values) {
            if (!value.equals(id)) {
                return false;
            }
        }

        return true;
    }

    private boolean isRealCustomerUserParam(String customerLogin, ContainerRequestContext context) {
        List<String> values = getPathParameterValues(UserService.USER_LOGIN_PARAM, context);

        if (values == null) {
            return true;
        }

        List<User> users = DBService.getCustomerUsers(DBService.getCustomerIdByLogin(customerLogin).toString());

        for (String login : values) {
            if (!users.stream().map(User::getLogin).anyMatch(login::equals)) {
                return false;
            }
        }
        return true;
    }
}